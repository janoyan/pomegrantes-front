import { AlmondPage } from './app.po';

describe('almond App', () => {
  let page: AlmondPage;

  beforeEach(() => {
    page = new AlmondPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
