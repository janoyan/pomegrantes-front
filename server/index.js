let express = require('express');
let app = express();
let path = require('path');
let publicFolder = __dirname + "/public/";
const PORT = process.env.PORT || 3000;

app.set('views', path.join(__dirname, '/views'));
app.engine('ejs', require('ejs').renderFile);
app.set('view engine', 'ejs');
// ADD COOKIE
app.use(function(req, res, next){
  res.cookie('basic-token', req.headers.authorization || '');
  next();
});
app.use(express.static(publicFolder));

// HANDLE NOT FOUND
app.use(function(req, res, next){
  res.render('404', { status: 404, url: req.url });
});

// HANDLE ERRORS
app.use(function(err, req, res, next){
  res.render('500', {
    status: err.status || 500,
    error: err
  });
});


app.listen(PORT, function () {
  console.log('Almond running on port ', PORT);
});
