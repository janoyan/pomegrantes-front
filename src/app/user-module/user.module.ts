import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule, MatButtonModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

// PrimeNG
import { AccordionModule } from 'primeng/components/accordion/accordion';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { TreeTableModule } from 'primeng/components/treetable/treetable';
import { ProgressBarModule } from 'primeng/components/progressbar/progressbar';
import { PanelModule } from 'primeng/components/panel/panel';
import { FileUploadModule } from 'ng2-file-upload';

// COMPONENTS
import { UploadComponent } from './components/upload/upload.component';
import { AnalyzeComponent } from './components/analyze/analyze.component';

import { SharedModule } from '../shared-module/shared.module';
import { DocumentViewComponent } from './components/document-view/document-view.component';
import { UserComponent } from './components/user/user.component';
import { TopicViewComponent } from './components/topic-view/topic-view.component';
import { NotableMatterViewComponent } from './components/notable-matter-view/notable-matter-view.component';
@NgModule({
  declarations: [
    UploadComponent,
    AnalyzeComponent,
    DocumentViewComponent,
    UserComponent,
    TopicViewComponent,
    NotableMatterViewComponent
  ],
  exports: [
    UploadComponent,
    AnalyzeComponent,
    DocumentViewComponent,
    UserComponent,
    TopicViewComponent,
    NotableMatterViewComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatButtonModule,
    RouterModule,
    FileUploadModule,
    AccordionModule,
    DataTableModule,
    PanelModule,
    TreeTableModule,
    ProgressBarModule
  ],
  providers: [],
  bootstrap: []
})

export class UserModule {}
