import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PdfViewComponent } from '../../../shared-module/components/pdf-view.component';
import * as Messages from '../../../constants/messages';
import {Util} from '../../../helpers/util';
import { DataService } from '../../../shared-module/services/data.service';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as _ from 'lodash';
import {Provision} from '../../../shared-module/models/provision.model';
import {DocumentType} from '../../../shared-module/models/document-type.model';
import {NotableMatter} from '../../../shared-module/models/notable-matter.model';
import {Topic} from '../../../shared-module/models/topic.model';

@Component({
  selector: 'app-document-view',
  templateUrl: './document-view.component.html',
  styleUrls: ['./document-view.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class DocumentViewComponent implements OnInit, OnDestroy {
  private routerSub: any;
  private documentContentId: string;
  private provisions: Provision[] = [];
  private topics: Topic[] = [];
  private documentTypes: DocumentType[] = [];
  private notableMatters: NotableMatter[] = [];
  public documentTypeName: string;
  public documentInfo: any;
  public topicsTree: any[] = [];
  public pdfView: PdfViewComponent;
  public textLevelHeight: number = 600;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private dataService: DataService,
              private messagebox: MessageboxService) {
    this.messagebox.showFullPreloader();
  }

  ngOnInit() {
    this.dataService.getProvisions().subscribe(
      (data: any) => this.provisions = data
    );
    this.dataService.getDocumentTypes().subscribe(
      (data: any) => this.documentTypes = data
    );
    this.dataService.getNotableMatters().subscribe(
      (data: any) => this.notableMatters = data
    );
    this.routerSub = this.route.params.subscribe(params => {
      this.documentContentId = params['id'];
      const files = this.dataService.analyseResult.files || [];
      this.documentInfo = files.find(item => item.document_content_id === this.documentContentId);
      if (!this.documentInfo) {
        this.router.navigate(['/']);
        return;
      }
      this.dataService.getTopics().subscribe(
        (data: any) => {
          const fullTopics = [];
          this.topics = data || [];
          this.documentInfo.topics.forEach(topic => {
            const child = this.topics.find(item => item.id === topic.topic_id);
            fullTopics.push(Object.assign({}, topic, {
              child_of: child ? child.child_of : null
            }));
          });
          this.topicsTree = Util.generateTopicsTree(fullTopics);
        }
      );
    });
    this.onResize();
  }
  ngOnDestroy() {
    this.routerSub.unsubscribe();
    this.messagebox.hideFullPreloader();
  }

  public onPdfInit(pdfView: PdfViewComponent) {
    this.pdfView = pdfView;
    this.pdfView.scrollerClass = '.right-document-content';
    pdfView.setToken(this.dataService.getToken());
    this.pdfView.renderPdf(this.dataService.getPdfUrlByContentId(this.documentContentId), err => {
      this.messagebox.hideFullPreloader();
      if (err) {
        return this.messagebox.error(Messages.ERR_CNT_GET_CNT)
      }
    });
  }

  public getProvisionName(provisionId: string): string {
    const provision: Provision = _.find(this.provisions, item => item.id === provisionId);
    return provision ? provision.name : provisionId;
  }
  public getDocumentTypeName(dtId: string): string {
    const documentType: DocumentType = _.find(this.documentTypes, item => item.id === dtId);
    return documentType ? documentType.name : dtId;
  }
  public getNotableMatterName(nmId: string): string {
    const notableMatter: NotableMatter = _.find(this.notableMatters, item => item.id === nmId);
    return notableMatter ? notableMatter.name : nmId;
  }
  public textSelected(e): void {
    this.pdfView.unmark();
  }
  public highlightText(texts: string[]): void {
    this.pdfView.highlightText(texts);
  }
  public getTopicName(topicId: string): string {
    const topic: Topic = _.find(this.topics, item => item.id === topicId);
    return topic ? topic.name : topicId;
  }

  // Private methods
  private onResize(): void {
    this.textLevelHeight = window.innerHeight - 65;
  }


}
