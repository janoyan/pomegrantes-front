import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotableMatterComponent } from './notable-matter.component';

describe('NotableMatterComponent', () => {
  let component: NotableMatterComponent;
  let fixture: ComponentFixture<NotableMatterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotableMatterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotableMatterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
