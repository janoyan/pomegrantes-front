import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../../shared-module/services/data.service';
import {Provision} from '../../../shared-module/models/provision.model';
import * as _ from 'lodash';
import {NotableMatter} from '../../../shared-module/models/notable-matter.model';

@Component({
  selector: 'app-notable-matter',
  templateUrl: './notable-matter-view.component.html',
  styleUrls: ['./notable-matter-view.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class NotableMatterViewComponent implements OnInit, OnDestroy {
  private provisions: Provision[] = [];
  private notableMatters: NotableMatter[] = [];
  public allNotableMatters: any[] = [];
  public selectedNotableMatter: any;
  public contextToShow: string;
  public textLevelHeight: number = 600;
  constructor(private dataService: DataService,
              private router: Router) { }

  ngOnInit() {
    if (_.isEmpty(this.dataService.analyseResult)) {
      this.router.navigate(['/']);
      return;
    }
    this.dataService.getProvisions().subscribe(
      (data: any) => this.provisions = data
    );
    this.dataService.getNotableMatters().subscribe(
      (data: any) => this.notableMatters = data
    );
    this.selectNotableMatter(this.dataService.selectedNotableMatter);
    const files = this.dataService.analyseResult.files || [];
    files.forEach(file => {
      this.allNotableMatters = this.allNotableMatters.concat(file.notableMatters);
    });
    this.onResize();
  }
  ngOnDestroy() {
    this.dataService.keepSelectedNotableMatter({});
  }

  public selectNotableMatter(nm): void {
    if (!nm || _.isEmpty(nm)) {
      return;
    }
    let tmpContext = nm.paragraph;
    nm.contexts.forEach(context => {
      tmpContext = tmpContext.replace(context, '<b>' + context + '</b>');
    });
    this.contextToShow = tmpContext;
    this.selectedNotableMatter = nm;
  }
  public getNotableMatterName(nmId: string): string {
    const nm: NotableMatter = _.find(this.notableMatters, item => item.id === nmId);
    return nm ? nm.name : nmId;
  }

  // Private methods
  private onResize(): void {
    this.textLevelHeight = window.innerHeight - 65;
  }

}
