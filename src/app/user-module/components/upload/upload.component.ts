import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import { environment } from '../../../../environments/environment';
import { DataService } from '../../../shared-module/services/data.service';
import { HttpEventType, HttpResponse, HttpHeaderResponse } from '@angular/common/http';
import * as $ from 'jquery';
import { Mock } from '../../../shared-module/services/mock.data';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})

export class UploadComponent implements OnInit {
  public filesToUpload: any[] = [];
  public filelist: FileItem[] = [];
  public progress: number = 0;
  public uploadsCompleted: boolean = false;
  public analyseButtonDisabled: boolean = false;
  public uploader: FileUploader = new FileUploader({
    url: environment.api.base + environment.api.analyse,
    allowedMimeType: [
      'application/pdf',
      'application/msword',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    ],
    autoUpload: false
  });
  public hasDropZoneOver: Boolean = false;

  constructor(public messagebox: MessageboxService,
              public router: Router,
              private dataService: DataService
  ) {}
  ngOnInit() {
    this.uploader.authToken = this.dataService.getToken();
    this.uploader.onWhenAddingFileFailed = () => {
      this.messagebox.error(Messages.ERR_ONLY_DOC);
    };
    this.uploader.onErrorItem = (f) => {
      f.remove();
      this.messagebox.error(Messages.ERR_WHILE_UPLOADING.replace('{{fname}}', f.file.name));
      const index = this.filelist.indexOf(f);
      if (index !== -1) {
        this.filelist.splice(index, 1);
        this.filelist = this.filelist.slice();
      }
    };
    this.uploader.onAfterAddingFile = (item) => {
      this.filesToUpload.push(item._file);
      this.filesToUpload = this.filesToUpload.slice();
      $('body').animate({ scrollTop: $('body').height() }, 'slow');
    };
  }
  public fileOver(e: any): void {
    this.hasDropZoneOver = e;
  }
  public beginAnalysis() {
    this.analyseButtonDisabled = true;
    const sub = this.dataService.uploadAnalyseFiles({files: this.filesToUpload}).subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
          if (this.progress === 100) {
            this.messagebox.showFullPreloader(Messages.INF_ANALYSING);
          }
        } else if (event instanceof HttpResponse) {
          this.dataService.keepAnalyseResult(event.body);
          this.router.navigate(['/analyze']);
          this.messagebox.hideFullPreloader();
          sub && sub.unsubscribe();
        } else if (event instanceof HttpHeaderResponse) {
          switch (event.status) {
            case 400:
              this.messagebox.error(Messages.ERR_NOT_MTCHS);
              this.resetData();
              this.messagebox.hideFullPreloader();
              sub && sub.unsubscribe();
              break;
            case 0:
              this.messagebox.error(Messages.ERR_CNT_ANLS);
              this.messagebox.hideFullPreloader();
              this.resetData();
              sub && sub.unsubscribe();
              break;
          }
        }
      });
  }
  public removeFile(index: number): void {
    if (index >= 0) {
      this.filesToUpload.splice(index, 1);
      this.filesToUpload = this.filesToUpload.slice();
    }
  }

  // Private methods
  private resetData(): void {
    this.filesToUpload = [];
    this.filelist = [];
    this.progress = 0;
    this.analyseButtonDisabled = false;
  }

}
