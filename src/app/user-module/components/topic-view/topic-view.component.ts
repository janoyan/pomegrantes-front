import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import {Provision} from '../../../shared-module/models/provision.model';
import {Topic} from '../../../shared-module/models/topic.model';
import {Util} from '../../../helpers/util';
import { DataService } from '../../../shared-module/services/data.service';

@Component({
  selector: 'app-topic-view',
  templateUrl: './topic-view.component.html',
  styleUrls: ['./topic-view.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class TopicViewComponent implements OnInit, OnDestroy {
  private provisions: Provision[] = [];
  private topics: Topic[] = [];
  public contextToShow: string;
  public topicsTree: any[] = [];
  public selectedTopic: any;
  public textLevelHeight: number = 600;
  constructor(private dataService: DataService,
              private router: Router) { }

  ngOnInit() {
    this.dataService.getProvisions().subscribe(
      (data: any) => this.provisions = data
    );
    if (_.isEmpty(this.dataService.analyseResult)) {
      this.router.navigate(['/']);
      return;
    }
    this.dataService.getTopics().subscribe(
      (data: any) => {
        const allTopics = this.getAllTopics();
        const fullTopics = [];
        this.topics = data || [];
        allTopics.forEach(topic => {
          const child = this.topics.find(item => item.id === topic.topic_id);
          fullTopics.push(Object.assign({}, topic, {
            child_of: child ? child.child_of : null
          }));
        });
        this.topicsTree = Util.generateTopicsTree(fullTopics);
        this.selectTopic(this.dataService.selectedTopic);
      }
    );
    this.onResize();
  }
  ngOnDestroy() {
    this.dataService.keepSelectedTopic({});
  }
  public getProvisionName(provisionId: string): string {
    const provision: Provision = _.find(this.provisions, item => item.id === provisionId);
    return provision ? provision.name : provisionId;
  }
  public selectTopic(topic): void {
    if (!topic || _.isEmpty(topic)) {
      return;
    }
    let tmpContext = topic.paragraph;
    topic.contexts.forEach(context => {
      tmpContext = tmpContext.replace(context, '<b>' + context + '</b>');
    });
    this.contextToShow = tmpContext;
    this.selectedTopic = topic;
  }

  // Private methods
  private onResize(): void {
    this.textLevelHeight = window.innerHeight - 65;
  }
  private getAllTopics() {
    let topics = [];
    const files = this.dataService.analyseResult.files || [];
    files.forEach(file => {
      topics = topics.concat(file.topics);
    });
    return _.uniqBy(topics, 'topic_id');
  }
  public getTopicName(topicId: string): string {
    const topic: Topic = _.find(this.topics, item => item.id === topicId);
    return topic ? topic.name : topicId;
  }



}
