import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import * as Messages from '../../../constants/messages';
import { Util } from '../../../helpers/util';
import { DataService } from '../../../shared-module/services/data.service';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as _ from 'lodash';
import * as arrayToTree from 'array-to-tree';
import {TransactionType} from '../../../shared-module/models/transaction-type.model';
import {Topic} from '../../../shared-module/models/topic.model';
import {NotableMatter} from '../../../shared-module/models/notable-matter.model';

@Component({
  selector: 'app-analyze',
  templateUrl: './analyze.component.html',
  styleUrls: ['./analyze.component.scss']
})
export class AnalyzeComponent implements OnInit, OnDestroy {
  public analyseList: any;
  public topics: Topic[];
  public topicsTree: any;
  public notableMatters: NotableMatter[];
  public transactionType: TransactionType = {};
  public allTopics: any;
  public allNotableMatters: any;
  constructor(private dataService: DataService,
              private messagebox: MessageboxService,
              private router: Router) {}

  ngOnInit() {
    if (_.isEmpty(this.dataService.analyseResult)) {
      this.messagebox.hideFullPreloader();
      this.router.navigate(['/']);
      return;
    }
    this.analyseList = this.generateAnalyseList();
    this.allTopics = this.getAllTopics();
    this.allNotableMatters = this.getAllNotableMatters();
    this.dataService.getTransactionType({
      transactionTypeId: this.analyseList.transaction_type_id
    }).subscribe(
      transactionType => {
        this.transactionType = transactionType;
      }
    );
    this.dataService.getTopics().subscribe(
      (data: any) => {
        const allTopics = this.getAllTopics();
        const fullTopics = [];
        this.topics = data || [];
        allTopics.forEach(topic => {
          const child = this.topics.find(item => item.id === topic.topic_id);
          fullTopics.push(Object.assign({}, topic, {
            child_of: child ? child.child_of : null
          }));
        });
        this.topicsTree = Util.generateTopicsTree(fullTopics);
      }
    );
    this.dataService.getNotableMatters().subscribe(
      (data: any) => this.notableMatters = data
    );
  }
  ngOnDestroy() {  }
  public getTopicName(topicId: string): string {
    const topic: Topic = _.find(this.topics, item => item.id === topicId);
    return topic ? topic.name : topicId;
  }
  public getNotableMatterName(nmId: string): string {
    const nm: NotableMatter = _.find(this.notableMatters, item => item.id === nmId);
    return nm ? nm.name : nmId;
  }
  public navigateToTopicView(topic): void {
    this.dataService.keepSelectedTopic(topic);
    this.router.navigate(['/analyze/topic']);
  }
  public navigateToNotableMatterView(nm): void {
    this.dataService.keepSelectedNotableMatter(nm);
    this.router.navigate(['/analyze/notable-matter']);
  }

  // Private methods
  private generateAnalyseList() {
    const result = {
      files: [],
      transaction_type_id: this.dataService.analyseResult['transaction_type_id']
    };
    const files = this.dataService.analyseResult.files || [];
    files.forEach(file => {
      const newItem: any = {
        file_name: file.file_name,
        document_content_id: file.document_content_id,
        document_type_id: file.document_type_id,
        topics: arrayToTree(file.topics, {
          parentProperty: 'child_of',
          customID: 'topic_id'
        }),
        notable_matters: file.notable_matters
      };
      newItem.topics = Util.getChildren(newItem.topics, []);
      result.files.push(newItem);
    });
    return result;
  }
  private getAllTopics() {
    let topics = [];
    const files = this.dataService.analyseResult.files || [];
    files.forEach(file => {
      topics = topics.concat(file.topics);
    });
    return _.uniqBy(topics, 'topic_id');
  }
  private getAllNotableMatters() {
    let nm = [];
    const files = this.dataService.analyseResult.files || [];
    files.forEach(file => {
      nm = nm.concat(file.notableMatters || []);
    });
    return _.uniqBy(nm, 'notable_matter_id');
  }
}
