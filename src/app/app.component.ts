import { Component } from '@angular/core';
import { MessageboxService } from './shared-module/services/messagebox.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public messagebox: MessageboxService) {}
}
