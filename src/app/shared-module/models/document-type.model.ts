export interface DocumentType {
  id?: string;
  name?: string;
  has_latest_model?: boolean;
}

export interface DocumentTypeLabel {
  label?: string;
  value?: DocumentType;
}
