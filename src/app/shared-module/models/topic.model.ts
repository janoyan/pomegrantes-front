export interface Topic {
  id?: string;
  name?: string;
  definition?: string[];
  displayDefinition?: string;
  transaction_type?: string;
  child_of?: string;
}

export interface TopicLabel {
  label?: string;
  value?: Topic;
}
