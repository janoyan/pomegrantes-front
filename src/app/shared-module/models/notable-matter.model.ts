export interface NotableMatter {
  id?: string;
  name?: string;
  transaction_type?: string;
  definition?: string;
  displayDefinition?: string;
  comments?: string;
}

export interface NotableMatterLabel {
  label?: string;
  value?: NotableMatter;
}
