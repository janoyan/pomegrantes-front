export interface DocumentRecord {
  id?: string;
  name?: string;
  document_types?: any[];
  file?: any;
  isSuccess?: boolean;
  isUploading?: boolean;
  progress?: number;
}
