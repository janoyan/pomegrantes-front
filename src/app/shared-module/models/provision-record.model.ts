export interface ProvisionRecord {
  id?: string,
  for_provisions: string[],
  in_document_record: string,
  selected_text: string,
  indices?: number[],
  coords?: number[][];
}
