export interface TransactionType {
  id?: string;
  name?: string;
  displayDefinition?: string;
  definition?: string;
}

export interface TransactionTypeLabel {
  label?: string;
  value?: TransactionType;
}
