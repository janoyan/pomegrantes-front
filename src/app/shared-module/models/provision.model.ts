export interface Provision {
  id?: string;
  name?: string;
  transaction_type?: string;
  child_of?: string;
  document_type?: string;
  has_latest_model?: boolean;
}

export interface ProvisionLabel {
  label?: string;
  value?: Provision;
}
