import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

const SHOW_TIMEOUT = 3000;
const OK_BUTTON = 'OK';

@Injectable()
export class MessageboxService {
  public fullPreloader: boolean = false;
  public fullPreloaderMessage: string;
  constructor (public snackBar: MatSnackBar) {}
  public success(message: string): void {
    this.snackBar.open(message, OK_BUTTON, {
      duration: SHOW_TIMEOUT,
      extraClasses: ['success-snackbar']
    });
  }
  public warning(message: string): void {
    this.snackBar.open(message, OK_BUTTON, {
      duration: SHOW_TIMEOUT,
      extraClasses: ['warning-snackbar']
    });
  }
  public error(message: string): void {
    this.snackBar.open(message, OK_BUTTON, {
      duration: SHOW_TIMEOUT,
      extraClasses: ['error-snackbar']
    });
  }
  public showFullPreloader(message?: string): void {
    this.fullPreloaderMessage = message || 'Loading..';
    this.fullPreloader = true;
  };
  public hideFullPreloader(): void {
    this.fullPreloaderMessage = '';
    this.fullPreloader = false;
  };
}
