import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './http.service';
import { Util } from '../../helpers/util';
import { environment } from '../../../environments/environment';
import { HttpEvent} from '@angular/common/http';
@Injectable()

export class DataService {
  private _analyseResult: any;
  private _selectedNotableMatter: any;
  private _selectedTopic: any;
  public get analyseResult(): any {
    return this._analyseResult || {};
  }
  public get selectedTopic(): any {
    return this._selectedTopic || {};
  }
  public get selectedNotableMatter(): any {
    return this._selectedNotableMatter || {};
  }
  constructor (public httpService: HttpService) {}
  public uploadDocumentRecordContent(options): Observable<HttpEvent<any>>  {
    return this.httpService.streamRequest(environment.api.base + environment.api.documentRecord +
      environment.api.documentRecordContent.replace(':id', options.id), options.file);
  }
  public uploadAnalyseFiles(options): Observable<HttpEvent<any>>  {
    return this.httpService.multipartRequest(environment.api.base + environment.api.analyse , options.files);
  }
  public keepAnalyseResult(result: any): void {
    this._analyseResult = result;
  }
  public keepSelectedTopic(topic: any): void {
    this._selectedTopic = topic;
  }
  public keepSelectedNotableMatter(nm: any): void {
    this._selectedNotableMatter = nm;
  }
  // GET LISTS
  /*--------------------------------*/
  public getDocumentTypes(options?): Observable<Response> {
    return this.httpService.get(environment.api.base + environment.api.documentTypes, {});
  }
  public getDocumentType(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.documentType + options.documentTypeId, {});
  }
  /*--------------------------------*/
  public getTransactionTypes(options?): Observable<Response> {
    return this.httpService.get(environment.api.base + environment.api.transactionTypes, {});
  }
  public getTransactionTypesByEvalution(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.transactionTypes +
      environment.api.getByEvalution.replace(':es', options.evalutionString), {});
  }
  public getTransactionType(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.transactionType + options.transactionTypeId, {});
  }
  /*--------------------------------*/
  public getProvisions(options?): Observable<Response> {
    return this.httpService.get(environment.api.base + environment.api.provisions, {});
  }
  public getProvisionsWithTransactionId(options, callback: Function) {
    // TODO: real request
    setTimeout(() => {
      callback(null, [
        {
          id: '484fg4d4g84b8d4',
          name: 'Provision1',
          transaction_type: 'object_id2',
          document_type: 'hka56dsvsdc5h52k'
        }
      ]);
    }, 650);
  }
  public getProvisionChildren(options, callback: Function) {
    // TODO: real request
    setTimeout(() => {
      callback(null, [
        {
          id: '484fg4d4g84b8d4',
          name: 'Provision1',
          transaction_type: 'object_id2',
          document_type: 'hka56dsvsdc5h52k'
        }
      ]);
    }, 650);
  }
  public getProvision(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.provision + options.provisionId, {});
  }
  /*--------------------------------*/
  public getTopics(options?): Observable<Response> {
    return this.httpService.get(environment.api.base + environment.api.topics, {});
  }
  public getTopicsByEvalution(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.topics +
      environment.api.getByEvalution.replace(':es', options.evalutionString), {});
  }
  public getTopicChildren(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.topic +
      environment.api.childrenOf.replace(':id', options.topicId), {});
  }
  public getTopic(options, callback: Function): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.topic + options.topicId, {});
  }
  /*--------------------------------*/
  public getNotableMatters(options?): Observable<Response> {
    return this.httpService.get(environment.api.base + environment.api.notableMatters, {});
  }
  public getNotableMattersWithTransactionId(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.notableMatters +
      environment.api.withTransactionId.replace(':id', options.transactionId), {});
  }
  public getNotableMattersByEvalution(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.notableMatters +
      environment.api.getByEvalution.replace(':es', options.evalutionString), {});
  }
  public getNotableMatter(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.notableMatter + options.notableMatterId, {});
  }
  /*--------------------------------*/
  public getPdfUrl(recordId: string): string {
    return environment.api.base + environment.api.documentRecord + recordId + '/content.pdf';
  }
  public getPdfUrlByContentId(contentId: string): string {
    return environment.api.base + environment.api.analyse + environment.api.documentContent + contentId + '.pdf';
  }
  public getDocumentRecords(options?): Observable<Response> {
    return this.httpService.get(environment.api.base + environment.api.documentRecords, {});
  }
  public getDocumentRecordContent(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.documentRecord +
      environment.api.documentRecordContent.replace(':id', options.id), {});
  }
  public getDocumentRecordById(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.documentRecord + options.id, {});
  }
  public getDocumentRecordLabelingInfo(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.documentRecord + options.documentId + '/labeling', {});
  }
  public getProvisionRecords(options?): Observable<Response> {
    return this.httpService.get(environment.api.base +
      environment.api.provisionRecords, {});
  }
  public getProvisionRecordsByProvision(options) {
    return this.httpService.get(environment.api.base +
      environment.api.provisionRecords + environment.api.byProvision + options.provisionId, {});
  }
  public getProvisionRecordById(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.provisionRecord + options.provisionRecordId, {});
  }
  public getProvisionRecordsByDocumentRecordId(options) {
    return this.httpService.get(environment.api.base +
      environment.api.provisionRecords + environment.api.inDocumentRecord + options.documentRecordId , {});
  }
  /*--------------------------------*/
  public getDTTEntriesByDocumentId(options): Observable<Response> {
    options = options || {};
    return this.httpService.get(environment.api.base +
      environment.api.documentRecordsEntrie + environment.api.documentRecord + options.documentId, {});
  }
  public getDTTEntriesByDocumentTypeId(options: {documentTypeId: string}) {
    return this.httpService.get(environment.api.base +
      environment.api.documentRecordsEntrie + environment.api.documentType + options.documentTypeId, {});
  }

  // CREATE
  public createDocumentTypes(options): Observable<Response> {
    options = options || [];
    return this.httpService.post(environment.api.base + environment.api.documentTypes, options);
  }
  public createTransactionType(options): Observable<Response> {
    return this.httpService.post(environment.api.base + environment.api.transactionTypes, options);
  }
  public createProvisions(options): Observable<Response> {
    options = options || [];
    return this.httpService.post(environment.api.base + environment.api.provisions, options);
  }
  public createTopic(options): Observable<Response> {
    options = options || [];
    return this.httpService.post(environment.api.base + environment.api.topics, options);
  }
  public createNotableMatter(options): Observable<Response> {
    options = options || [];
    return this.httpService.post(environment.api.base + environment.api.notableMatters, options);
  }
  public createDocumentRecords(options): Observable<Response> {
    options = options || [];
    return this.httpService.post(environment.api.base + environment.api.documentRecords, options);
  }
  public createProvisionRecords(options): Observable<Response> {
    options = options || [];
    return this.httpService.post(environment.api.base + environment.api.provisionRecords, options);
  }
  public createDTTEntries(options): Observable<Response> {
    options = options || [];
    return this.httpService.post(environment.api.base + environment.api.documentRecordsEntrie, options);
  }
  // UPDATE
  public updateDocumentType(options): Observable<Response> {
    options = options || [];
    return this.httpService.put(environment.api.base + environment.api.documentTypes, options);
  }
  public updateTransactionType(options) {
    return this.httpService.put(environment.api.base + environment.api.transactionTypes, options);
  }
  public updateProvision(options): Observable<Response> {
    return this.httpService.put(environment.api.base + environment.api.provisions, options);
  }
  public updateTopic(options): Observable<Response> {
    return this.httpService.put(environment.api.base + environment.api.topics, options);
  }
  public updateNotableMatter(options): Observable<Response> {
    options = options || [];
    return this.httpService.put(environment.api.base + environment.api.notableMatters, options);
  }

  // REMOVE
  public removeDocumentType(options): Observable<Response> {
    options = options || [];
    return this.httpService.delete(environment.api.base + environment.api.documentTypes, options);
  }
  public removeDocumentRecord(options): Observable<Response> {
    options = options || [];
    return this.httpService.delete(environment.api.base + environment.api.documentRecords, options);
  }
  public removeTransactionType(options): Observable<Response> {
    return this.httpService.delete(environment.api.base + environment.api.transactionTypes, options);
  }
  public removeProvision(options): Observable<Response> {
    return this.httpService.delete(environment.api.base + environment.api.provisions, options);
  }
  public removeProvisionRecord(options): Observable<Response> {
    return this.httpService.delete(environment.api.base + environment.api.provisionRecords, options);
  }
  public removeTopic(options): Observable<Response> {
    return this.httpService.delete(environment.api.base + environment.api.topics, options);
  }
  public removeNotableMatter(options): Observable<Response> {
    options = options || [];
    return this.httpService.delete(environment.api.base + environment.api.notableMatters, options);
  }
  public removeDTTEntries(options): Observable<Response> {
    options = options || [];
    return this.httpService.delete(environment.api.base + environment.api.documentRecordsEntrie, options);
  }

  // TRAIN
  public retrainDocumentType(options): Observable<Response> {
    return this.httpService.post(environment.api.base + environment.api.documentType + options.documentTypeId + '/training' , {});
  }
  public retrainProvision(options): Observable<Response> {
    return this.httpService.post(environment.api.base + environment.api.provision + options.provisionId + '/training' , {});
  }

  public getToken(): string {
    return this.httpService.token;
  }
}
