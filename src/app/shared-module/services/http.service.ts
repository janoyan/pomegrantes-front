import { Injectable } from '@angular/core';
import {HttpRequest, HttpHeaders, HttpClient, HttpEvent} from '@angular/common/http';
import { Http, Headers, RequestMethod, URLSearchParams} from '@angular/http';
import { Cookie } from 'ng2-cookies';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';

@Injectable()
export class HttpService {

  private header: Headers = null;
  public get token(): string {
    return Cookie.get('basic-token');
  }
  public static parseResponse(response): any {
    try {
      return response.json();
    } catch (err) {
      return response.text();
    }
  }

  constructor (private http: Http, private commonHttp: HttpClient) {
    this.createHeader()
  }
  public createHeader(): void {
    if (!this.header) {
      this.header = new Headers();
    }
    this.header.delete('Content-Type');
    this.header.delete('Authorization');
    this.header.append('Content-Type', 'application/json');
    this.header.append('Authorization', this.token);
  }
  public post(uri: string, body): Observable<Response> {
    return this.request(RequestMethod.Post, uri, body);
  }
  public get(uri: string, body): Observable<Response>  {
    const search = new URLSearchParams();
    for (const key in body) {
      if (body.hasOwnProperty(key)) {
        search.append(key, body[key]);
      }
    }
    return this.http.get (uri, {
      headers: this.header,
      search: search
    }).map(HttpService.parseResponse);
  }
  public put(uri: string, body): Observable<Response>  {
    return this.request(RequestMethod.Put, uri, body);
  }
  public delete(uri: string, body): Observable<Response>  {
    return this.request(RequestMethod.Delete, uri, body);
  }
  public streamRequest(uri: string, file): Observable<HttpEvent<any>>  {
    const headers = new HttpHeaders({
      'Content-Type': file.type,
      'Authorization': this.token
    });
    const req = new HttpRequest('PUT', uri, file, {
      headers: headers,
      reportProgress: true,
    });
    return this.commonHttp.request(req);
  }
  public multipartRequest(uri: string, files: any[]): Observable<HttpEvent<any>>  {
    const headers = new HttpHeaders({
      'Authorization': this.token
    });
    const formData = new FormData();
    files.forEach(file => {
      formData.append('file[]', file);
    });
    const req = new HttpRequest('PUT', uri, formData, {
      headers: headers,
      reportProgress: true,
    });
    return this.commonHttp.request(req);
  }

  // Private methods
  private request(method: RequestMethod, uri: string, body: Object): Observable<Response>  {
    return this.http.request(uri, {
      method: method,
      headers: this.header,
      body: body
    }).map(HttpService.parseResponse);
  }
}
