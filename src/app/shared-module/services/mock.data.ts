export class Mock {
  public static analyse: any = {
    "transaction_type_id": "TT_11",
    "files": [
      {
        "file_name": "somefile.docx",
        "document_content_id": "ab99d816-9d3c-410e-487-df50a4f46f15",
        "document_type_ids": ["DT_777"],
        "topics": [
          {
            "topic_id": "T_33",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          },
          {
            "topic_id": "T_34",
            "child_of": "T_33",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          },
          {
            "topic_id": "T_35",
            "child_of": "T_33",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          }
        ],
        "notableMatters": [
          {
            "notable_matter_id": "NM_22",
            "provision_id": "P_33",
            "paragraph": "Some string Some string from paragraph to highlight Some Some string from paragraph to highlight from paragraph to highlight to highlight paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          }
        ]
      },
      {
        "file_name": "another.docx",
        "document_content_id": "dvscsacs-9d3c-410e-9d36-df5046f15",
        "document_type_ids": ["DT_777"],
        "topics": [
          {
            "topic_id": "T_33",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          },
          {
            "topic_id": "T_34",
            "child_of": "T_33",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          },
          {
            "topic_id": "T_35",
            "child_of": "T_33",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          }
        ],
        "notableMatters": [
          {
            "notable_matter_id": "NM_22",
            "provision_id": "P_33",
            "paragraph": "Matched Another Another string Another string from same paragraph to highlight same Another string from same paragraph to highlight to highlight from same paragraph to highlight",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          }
        ]
      },
      {
        "file_name": "yessssss.docx",
        "document_content_id": "ab99d816-9d3c-410e-9d36-df50a48578f15",
        "document_type_ids": ["DT_777"],
        "topics": [
          {
            "topic_id": "T_33",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          },
          {
            "topic_id": "T_38",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "child_of": "T_33",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          },
          {
            "topic_id": "T_39",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "child_of": "T_38",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          },
          {
            "topic_id": "T_34",
            "child_of": "T_33",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          },
          {
            "topic_id": "T_35",
            "child_of": "T_33",
            "provision_id": "P_66",
            "paragraph": "Matched paragraph",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          }
        ],
        "notableMatters": [
          {
            "notable_matter_id": "NM_255",
            "provision_id": "P_33",
            "paragraph": "Matched Another string from same Another string from same paragraph to highlight Another string from same paragraph to highlight to highlight Another string from same paragraph to highlight",
            "contexts": [
              "Some string from paragraph to highlight",
              "Another string from same paragraph to highlight"
            ]
          }
        ]
      }
    ]
  }
}
