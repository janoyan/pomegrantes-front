import { NgModule } from '@angular/core';
// SERVICES
import { HttpService } from './services/http.service';
import { DataService } from './services/data.service';
import { MessageboxService } from './services/messagebox.service';
import { PdfViewComponent} from './components/pdf-view.component';
@NgModule({
  declarations: [PdfViewComponent],
  exports: [PdfViewComponent],
  imports: [
  ],
  providers: [
    HttpService,
    MessageboxService,
    DataService
  ],
  bootstrap: []
})

export class SharedModule {}
