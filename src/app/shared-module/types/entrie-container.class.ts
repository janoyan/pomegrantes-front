import * as _ from 'lodash';

export class EntrieContainer<T> {
  private _initialEntries: T[];
  private _entriesToInsert: T[];
  private _entriesToRemove:  T[];

  public get list(): T[] {
    return this._entriesToInsert;
  }
  public get insertList(): T[] {
    return _.filter(this._entriesToInsert, item => !_.find(this._initialEntries, fitem => _.isEqual(item, fitem)));
  }
  public get removeList(): T[] {
    return _.filter(this._entriesToRemove, item => _.find(this._initialEntries, fitem => _.isEqual(item, fitem)));
  }

  constructor(initialEntries?: T[]) {
    this._initialEntries = initialEntries || [];
    this._entriesToInsert = Object.assign([], initialEntries);
    this._entriesToRemove = [];
  }

  public insert(entrie: T): void {
    if (this._existsInInsertArray(entrie) !== -1) {
      return;
    }
    const index = this._existsInRemoveArray(entrie);
    if (index !== -1) {
      this._entriesToRemove.splice(index, 1);
    }
    this._entriesToInsert.push(entrie);
  }
  public remove(entrie: T): void {
    if (this._existsInRemoveArray(entrie) !== -1) {
      return;
    }
    const index = this._existsInInsertArray(entrie);
    if (index !== -1) {
      this._entriesToInsert.splice(index, 1);
    }
    this._entriesToRemove.push(entrie);
  }
  public reset(initialEntries?: T[]): void {
    this._initialEntries = initialEntries || [];
    this._entriesToInsert = Object.assign([], initialEntries);
    this._entriesToRemove = [];
  }

  // Private methods
  private _existsInInsertArray(entrie: T): number {
    const found: T = this._entriesToInsert.find(item => _.isEqual(item, entrie));
    return found ? this._entriesToInsert.indexOf(found) : -1;
  }
  private _existsInRemoveArray(entrie: T): number {
    const found: T = this._entriesToRemove.find(item => _.isEqual(item, entrie));
    return found ? this._entriesToRemove.indexOf(found) : -1;
  }

}
