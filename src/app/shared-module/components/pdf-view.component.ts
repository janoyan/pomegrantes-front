import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { PDFJS } from 'pdfjs-dist/build/pdf.js';
import { PDFViewer} from 'pdfjs-dist/lib/web/pdf_viewer.js';
import { PDFFindController } from 'pdfjs-dist/lib/web/pdf_find_controller.js';
import * as Mark from 'mark.js';
import * as $ from 'jquery';
import * as _ from 'lodash';
import * as Messages from '../../constants/messages';
import { MessageboxService } from '../../shared-module/services/messagebox.service';
PDFJS.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
PDFJS.disableWorker = true;

@Component({
  selector: 'app-pdf-view',
  template: '<div class="html-viewer"><div id="viewerContainer"><div id="viewer" ></div></div></div>',
  host: {
    '(window:resize)': 'correctTextLayers()'
  }
})
export class PdfViewComponent implements OnInit {
  @Output() onInit: EventEmitter<any> = new EventEmitter();
  private pdfViewer: any;
  private token: string;
  private pdfFindController: PDFFindController;
  private readonly lastHighlightedElements: any[] = [];
  private _scrollerClass: string = '.html-viewer';
  private markInstance: any;
  public set scrollerClass(val) { this._scrollerClass = (val || this._scrollerClass); }
  constructor(private messagebox: MessageboxService) {
  }
  ngOnInit() {
    this.onInit.emit(this);
    this.markInstance = new Mark(document.querySelector('.html-viewer'));
  }
  public renderPdf(pdfUrl: string, callback?: Function): void {
    const self = this;
    const container = document.getElementById('viewerContainer');
    const viewer = document.getElementById('viewer');
    this.pdfViewer = new PDFViewer({
      container: container,
      viewer: viewer
    });
    this.pdfFindController = new PDFFindController({pdfViewer: this.pdfViewer });
    this.pdfViewer.setFindController(this.pdfFindController);
    container.addEventListener('pagesinit', () => {
      this.pdfViewer.currentScaleValue = 'page-width';
    });
    container.addEventListener('pagesloaded', () => {
      if (_.isFunction(callback)) {
        callback();
      }
    });
    container.addEventListener('textlayerrendered', () => {
      self.correctTextLayers();
      self.disableHyperlinks();
    });
    PDFJS.getDocument({
      url: pdfUrl,
      httpHeaders: {'Authorization': this.token}
    }).then((pdfDocument) => {
      this.pdfViewer.setDocument(pdfDocument);
    }).catch(err => {
      callback(err);
    });
  }
  public hideHighlight(): void {
    while (this.lastHighlightedElements.length) {
      this.lastHighlightedElements.pop().remove();
    }
  }
  public setToken(token: string): void {
    this.token = token;
  }
  public showHighlight(selected) {
    const pageIndex = 0;
    const page = this.pdfViewer.getPageView(pageIndex);
    const pageElement = page.canvas.parentElement;
    const viewport = page.viewport;
    let firstElement;
    let lastSizes: any;
    selected.forEach(rect => {
      const bounds = viewport.convertToViewportRectangle(rect);
      const el = document.createElement('div');
      const sizes = {
        left: Math.min(bounds[0], bounds[2]),
        top: Math.min(bounds[1], bounds[3]),
        width: Math.abs(bounds[0] - bounds[2]),
        height: Math.abs(bounds[1] - bounds[3])
      };
      if (this.isSamePoints(sizes, lastSizes)) {
        return;
      }
      lastSizes = sizes;
      el.setAttribute('style', 'position: absolute; background-color: rgba(238, 170, 0, .3);' +
        'left:' + sizes.left + 'px; top:' + sizes.top + 'px;' +
        'width:' + sizes.width + 'px; height:' + sizes.height + 'px;');
      pageElement.appendChild(el);
      if (!firstElement) {
        firstElement = el;
      }
      this.lastHighlightedElements.push(el);
    });
    const htmlViewer = $('.html-viewer');
    htmlViewer.animate({
      scrollTop: htmlViewer.scrollTop() + ($(firstElement).offset().top - 100)
    }, 'slow');
  }
  public highlightText(texts: string[]): void {
    const self = this;
    this.markInstance.unmark();
    let queries = [];
    const coordinates = {};
    texts.forEach(text => {
      queries = queries.concat(text.split('\n'));
    });
    this.markInstance.mark(queries, {
      caseSensitive: true,
      separateWordSearch: false,
      acrossElements: true,
      ignoreJoiners: true,
      ignorePunctuation: ['\n'],
      each: e => {
        const pageNumber = $(e).parents().eq(2).attr('data-page-number');
        if (!coordinates[pageNumber]) {
          coordinates[pageNumber] = [];
        }
        coordinates[pageNumber].push({
          top: $(e).offset().top,
          element: e
        });
      },
      done: (count) => {
        if (count < 1) {
          self.messagebox.warning(Messages.WRN_CNT_FND_PRG);
          return;
        }
        console.log(this.getBestMatchedElementsGroup(coordinates, queries.length), 'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC');
        if (!self.scrollToHighlight()) {
          setTimeout(() => {
            if (!self.scrollToHighlight()) {
              self.messagebox.warning(Messages.WRN_CNT_FND_PRG);
            }
          }, 1000);
        }
      }
    });
  }
  public getHightlightCoords() {
    const pageIndex = this.pdfViewer.currentPageNumber - 1;
    const page = this.pdfViewer.getPageView(pageIndex);
    const pageRect = page.canvas.getClientRects()[0];
    const selectionRects = window.getSelection().getRangeAt(0).getClientRects();
    const viewport = page.viewport;
    const selected = _.map(selectionRects, (r) => {
      return viewport.convertToPdfPoint(r.left - pageRect.left, r.top - pageRect.top).concat(
        viewport.convertToPdfPoint(r.right - pageRect.left, r.bottom - pageRect.top));
    });
    return selected;
  }
  public unmark(): void {
    this.markInstance.unmark();
  }
  // Private methods
  private getBestMatchedElementsGroup(coordinates, queriesLength) {
    const DIFF = 25;
    const pages = Object.keys(coordinates);
    const groups: any[] = [];
    let groupIndex = 0;
    pages.forEach(pageNumber => {
      let previous;
      _.orderBy(coordinates[pageNumber], ['top'], ['asc']).forEach( item => {
        if (_.isUndefined(previous) || Math.abs(item.top - previous) > DIFF) {
          ++ groupIndex;
          groups[groupIndex] = {
            page: pageNumber,
            firstElement: item.element,
            elements: [item.element]
          };
          previous = item.top;
        } else {
          groups[groupIndex].elements.push(item);
        }
      });
    });
    let bestResult = {
      diff: Math.abs(groups[1].elements.length - queriesLength),
      group: groups[1]
    };
    groups.forEach(group => {
      const diff = Math.abs(group.elements.length - queriesLength);
      if (bestResult.diff > diff) {
        bestResult = {
          diff: diff,
          group: group
        };
      }
    });
    console.log(bestResult, '=======Best');
    return coordinates;
  }
  private correctTextLayers(): void {
    $('div.page').each((i, elem) => {
      const pos = $(elem).position();
      $(elem).find('.textLayer').css({top: pos.top, left: pos.left});
    });
  }
  private disableHyperlinks() {
    $('.html-viewer a[class!="internalLink"]').on('click', (event) => {
      event.preventDefault();
    });
  }
  private isSamePoints(sizes1: any, sizes2: any): boolean {
    if (!sizes1 || !sizes2) {
      return false;
    }
    const diff = 3.5;
    return (Math.abs(Math.abs(sizes1.left) - Math.abs(sizes2.left)) < diff) &&
      (Math.abs(Math.abs(sizes1.top) - Math.abs(sizes2.top)) < diff) &&
      (Math.abs(Math.abs(sizes1.width) - Math.abs(sizes2.width)) < diff) &&
      (Math.abs(Math.abs(sizes1.height) - Math.abs(sizes2.height)) < diff);
  }
  private scrollToHighlight(): boolean {
    const offset = $($('mark').eq(0)).offset();
    if (!offset) {
      return false;
    }
    const htmlViewer = $(this._scrollerClass);
    htmlViewer.animate({
      scrollTop: htmlViewer.scrollTop() + (offset.top - 100)
    }, 'slow');
    return true;
  }
}
