import * as _ from 'lodash';
import * as arrayToTree from 'array-to-tree';
export const SPLIT_CHARACTER = ' ';
export const CONFLICT_STATUS_CODE = 409;

export class Util {
  public static isLogicalOperator(operator: string): boolean {
    return operator === 'AND' || operator === 'OR' || operator === 'NOT';
  }
  public static isValidDefinition(definition: string): boolean {
    if (!definition.length) {
      return true;
    }
    const defArray = definition.split(SPLIT_CHARACTER);
    const lastCharacter = defArray[defArray.length ? defArray.length - 1 : 0];
    if (Util.isLogicalOperator(lastCharacter)) {
      return false;
    }
    if (defArray.filter(item => item === '(').length !== defArray.filter(item => item === ')').length) {
      return false;
    }
    return true;
  }
  public static getChildren(data, container): any {
    if (_.isEmpty(data)) {
      return null;
    }
    data.forEach(item => {
      container.push({
        data: item,
        children: this.getChildren(item.children, [])
      });
    });
    return container;
  }
  public static generateTopicsTree(topics: any[]): any[] {
    let result = arrayToTree(topics, {
      parentProperty: 'child_of',
      customID: 'topic_id'
    });
    result = Util.getChildren(result, []);
    return result;
  }
}
