import { Routes } from '@angular/router';

// USER COMPONENTS
import { UploadComponent } from './user-module/components/upload/upload.component';
import { UserComponent } from './user-module/components/user/user.component';
import { DocumentViewComponent } from './user-module/components/document-view/document-view.component';
import { TopicViewComponent } from './user-module/components/topic-view/topic-view.component';
import { NotableMatterViewComponent } from './user-module/components/notable-matter-view/notable-matter-view.component';

// TRAINER COMPONENTS
import { AppComponent } from './app.component';
import { ListComponent } from './trainer-module/components/list/list.component';
import { DocumentTypesComponent } from './trainer-module/components/document-types/document-types.component';
import { TransactionTypesComponent } from './trainer-module/components/transaction-types/transaction-types.component';
import { TopicsComponent } from './trainer-module/components/topics/topics.component';
import { ProvisionsComponent } from './trainer-module/components/provisions/provisions.component';
import { NotableMattersComponent } from './trainer-module/components/notable-matters/notable-matters.component';
import { DocumentTypesRecordsComponent } from './trainer-module/components/document-types-records/document-types-records.component';
import { ProvisionsRecordsComponent } from './trainer-module/components/provisions-records/provisions-records.component';
import { AnalyzeComponent } from './user-module/components/analyze/analyze.component';
import { LabelingComponent } from './trainer-module/components/labeling/labeling.component';
import { NotFoundComponent } from './trainer-module/components/not-found/not-found.component';
import { TrainerComponent } from './trainer-module/components/trainer/trainer.component';


// ROUTES
export const AppRoutes: Routes = [
  {
    path: '',
    component: UserComponent,
    children: [
      {
        path: '',
        component: UploadComponent
      },
      {
        path: 'upload',
        component: UploadComponent
      },
      {
        path: 'analyze',
        component: AnalyzeComponent
      },
      {
        path: 'analyze/document/:id',
        component: DocumentViewComponent
      },
      {
        path: 'analyze/topic',
        component: TopicViewComponent
      },
      {
        path: 'analyze/notable-matter',
        component: NotableMatterViewComponent
      }
    ]
  },
  {
    path: 'training',
    component: TrainerComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'document-types',
        component: DocumentTypesComponent
      },
      {
        path: 'document-types/:id',
        component: DocumentTypesRecordsComponent
      },
      {
        path: 'transaction-types',
        component: TransactionTypesComponent
      },
      {
        path: 'notable-matters',
        component: NotableMattersComponent
      },
      {
        path: 'topics',
        component: TopicsComponent
      },
      {
        path: 'provisions',
        component: ProvisionsComponent
      },
      {
        path: 'provisions/:id',
        component: ProvisionsRecordsComponent
      },
      {
        path: 'labeling/:id',
        component: LabelingComponent
      },
      {
        path: 'labeling/:id/:prId',
        component: LabelingComponent
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
