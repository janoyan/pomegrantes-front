import { Component, OnInit } from '@angular/core';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import { DataService } from '../../../shared-module/services/data.service';
import { Provision, ProvisionLabel } from '../../../shared-module/models/provision.model';
import { TransactionTypeLabel, TransactionType } from '../../../shared-module/models/transaction-type.model';
import { DocumentTypeLabel, DocumentType } from '../../../shared-module/models/document-type.model';

@Component({
  selector: 'app-provisions',
  templateUrl: './provisions.component.html',
  styleUrls: ['./provisions.component.scss']
})
export class ProvisionsComponent implements OnInit {
  public transactionTypes: TransactionTypeLabel[] = [];
  public documentTypes: DocumentTypeLabel[];
  public provisions: Provision[];
  public availableProvisions: ProvisionLabel[];
  public selectedProvision: Provision = {};
  public selectedTransactionType: TransactionType = {};
  public selectedParentProvision: Provision = {};
  public selectedDocumentType: DocumentType = {};
  public isParentProvision: string[] = [];
  public provisionsLoading: boolean = true;
  public showTypeEditWindow: boolean = false;

  constructor(public messagebox: MessageboxService,
              public dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.getTransactionTypes().subscribe(
      (data: any) => {
        this.transactionTypes = [];
        data.forEach(item => {
          this.transactionTypes.push({
            label: item.name,
            value: item
          });
        });
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_TTS);
      }
    );
    this.dataService.getDocumentTypes().subscribe(
      (data: any) => {
        this.documentTypes = [];
        data.forEach(item => {
          this.documentTypes.push({
            label: item.name,
            value: item
          });
        });
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_DTS);
      }
    );
    this.refreshTransactionTypes();
  }
  public newProvisionType(): void {
    this.clearSelectedFields();
    this.availableProvisions = [];
    this.provisions.forEach(item => {
      this.availableProvisions.push({
        label: item.name,
        value: item
      });
    });
    this.showTypeEditWindow = true;
  }
  public getTransactionName(pv: Provision): string {
    const tt = this.transactionTypes.find(item => item.value.id === pv.transaction_type);
    return tt && tt.value ? tt.value.name : '';
  }
  public editProvision(pv: Provision): void {
    this.clearSelectedFields();
    this.selectedProvision = Object.assign({}, pv);
    this.selectedDocumentType = (this.documentTypes.find(item => item.value.id === pv.document_type) || {}).value || {};
    this.selectedTransactionType = (this.transactionTypes.find(item => item.value.id === pv.transaction_type) || {}).value || {};
    if (!pv.child_of) {
      this.selectedParentProvision = {};
      this.isParentProvision = ['isParentProvision'];
    } else {
      this.selectedParentProvision = (this.availableProvisions.find(item => item.value.id === pv.child_of) || {}).value || {};
      this.isParentProvision = [];
    }
    this.availableProvisions = [];
    this.provisions.forEach(item => {
      if (item.id === pv.id) {
        return;
      }
      this.availableProvisions.push({
        label: item.name,
        value: item
      });
    });
    this.showTypeEditWindow = true;
  }
  public saveProvision(provision: Provision): void {
    if (!provision.name) {
      this.messagebox.warning(Messages.WRN_PLS_FLL_PVN);
      return;
    }
    if (!this.selectedTransactionType || !this.selectedTransactionType.id) {
      this.messagebox.warning(Messages.WRN_PLS_SLC_TT);
      return;
    }
    provision.transaction_type = this.selectedTransactionType.id;
    provision.document_type = this.selectedDocumentType.id;
    if (!this.isParentProvision.length) {
      provision.child_of = this.selectedParentProvision.id;
    } else {
      delete provision.child_of;
    }
    // Update
    this.provisionsLoading = true;
    this.showTypeEditWindow = false;
    if (provision.id) {
      this.dataService.updateProvision([provision])
        .finally(() => {
          this.provisionsLoading = false;
        })
        .subscribe(
          data => {
            this.messagebox.success(Messages.SCS_PV_SCS_UPDT);
            this.refreshTransactionTypes();
          },
          err => {
            this.messagebox.error(Messages.ERR_CNT_UPDT_PV);
          }
        );
      return;
    }
    // Create new
    this.dataService.createProvisions([provision])
      .finally(() => {
        this.provisionsLoading = false;
      })
      .subscribe(
        data => {
          this.messagebox.success(Messages.SCS_PV_SCS_CRT);
          this.refreshTransactionTypes();
        },
        err => {
          this.messagebox.error(Messages.ERR_CNT_CRT_PV);
        }
      );
    return;
  }
  public removeProvision(provision: Provision): void {
    this.provisionsLoading = true;
    this.dataService.removeProvision([{id: provision.id}])
      .finally(() => {
        this.provisionsLoading = false;
      })
      .subscribe(
        data => {
          const index: number = this.provisions.indexOf(provision);
          if (index !== -1) {
            this.provisions.splice(index, 1);
            this.provisions = this.provisions.slice();
          }
          this.messagebox.success(Messages.SCS_PV_SCS_RMV);
        },
        err => {
          if (err.status === 409 ) {
            this.messagebox.error(Messages.ERR_PV_HAS_REL);
            return;
          }
          this.messagebox.error(Messages.ERR_CNT_RM_PV);
        }
      );
  }
  public onDTDropdownFocus(): void {
    if (!this.documentTypes || !this.documentTypes.length) {
      this.messagebox.warning(Messages.WRN_NO_AVLB_DT);
    }
  }
  public onParentDropdownFocus(): void {
    if (!this.availableProvisions || !this.availableProvisions.length) {
      this.messagebox.warning(Messages.WRN_NO_AVLB_PVS);
    }
  }
  public onTransactionDropdownFocus(): void {
    if (!this.transactionTypes || !this.transactionTypes.length) {
      this.messagebox.warning(Messages.WRN_NO_AVLB_TT);
    }
  }
  public retrainProvisionType(provision: Provision): void {
    this.messagebox.showFullPreloader(Messages.INF_RTR_PV);
    this.dataService.retrainProvision({provisionId: provision.id})
      .finally(() => {
        this.messagebox.hideFullPreloader();
      })
      .subscribe(
        () => {
          provision.has_latest_model = true;
        },
        err => {
          this.messagebox.error(Messages.ERR_CNT_RTR_PV)
        }
      );
  }

  // Private methods
  private clearSelectedFields(): void {
    this.selectedProvision = {};
    this.selectedParentProvision = {};
    this.selectedDocumentType = {};
    this.selectedTransactionType = {};
    this.isParentProvision = [];
  }
  private refreshTransactionTypes(): void {
    this.provisionsLoading = true;
    this.dataService.getProvisions()
      .finally(() => {
        this.provisionsLoading = false;
      })
      .subscribe(
        (data: any) => {
          this.provisions = data;
          this.availableProvisions = [];
          data.forEach(item => {
            this.availableProvisions.push({
              label: item.name,
              value: item
            });
          });
        },
        err => {
          return this.messagebox.error(Messages.ERR_CNT_LOAD_PVS);
        }
      );
  }
}
