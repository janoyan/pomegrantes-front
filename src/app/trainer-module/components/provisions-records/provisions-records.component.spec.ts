import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvisionsRecordsComponent } from './provisions-records.component';

describe('ProvisionsRecordsComponent', () => {
  let component: ProvisionsRecordsComponent;
  let fixture: ComponentFixture<ProvisionsRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvisionsRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvisionsRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
