import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import { DataService } from '../../../shared-module/services/data.service';
import { ProvisionRecord } from '../../../shared-module/models/provision-record.model';
import { DocumentRecord } from '../../../shared-module/models/document-record.model';

@Component({
  selector: 'app-provisions-records',
  templateUrl: './provisions-records.component.html',
  styleUrls: ['./provisions-records.component.scss']
})
export class ProvisionsRecordsComponent implements OnInit, OnDestroy {
  private routerSub: any;
  private provisionId: string;
  public records: ProvisionRecord[];
  public documentRecords: DocumentRecord[] = [];
  public recordsLoading = true;
  constructor(public messagebox: MessageboxService,
              public dataService: DataService,
              public route: ActivatedRoute) {}
  ngOnInit() {
    this.routerSub = this.route.params.subscribe(params => {
      this.provisionId = params['id'];
      this.dataService.getDocumentRecords().subscribe(
        (data: any) => {
          this.documentRecords = data;
          this.refreshProvisionRecords();
        },
        err => {
          this.messagebox.error(Messages.ERR_CNT_LOAD_DTD)
        }
      );
    });
  }
  ngOnDestroy() {
    this.routerSub.unsubscribe();
  }
  public removeRecord(dt): void {
    this.recordsLoading = true;
    this.dataService.removeProvisionRecord([dt])
      .finally(() => {
        this.refreshProvisionRecords();
      })
      .subscribe(null,
      err => {
        return this.messagebox.error(Messages.ERR_CNT_RM_DTD);
      }
    );
  }
  public getDocumentRecordName(id: string): string  {
    const dt = this.documentRecords.find(item => item.id === id);
    return dt ? dt.name : '';
  }
  // Private methods
  private refreshProvisionRecords(): void {
    this.dataService.getProvisionRecordsByProvision({provisionId: this.provisionId})
      .finally(() => {
        this.recordsLoading = false;
      })
      .subscribe(
        (data: any) => {
          this.records = data;
        },
        err => {
          if (err.status === 409 ) {
            this.messagebox.error(Messages.ERR_PVR_HAS_REL);
            return;
          }
          return this.messagebox.error(Messages.ERR_CNT_LOAD_PTD);
        }
      )
  }
}
