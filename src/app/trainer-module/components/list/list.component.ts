import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpEventType, HttpResponse, HttpHeaderResponse } from '@angular/common/http';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import { DataService } from '../../../shared-module/services/data.service';
import { DocumentRecord } from '../../../shared-module/models/document-record.model';
import * as async from 'async';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  public readonly allowedMimeTypes: string[] = [
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/pdf'];
  public fileList: DocumentRecord[];
  public fileListIsLoading: boolean;
  constructor(public messagebox: MessageboxService,
              public dataService: DataService,
              public router: Router) {}

  ngOnInit() {
    this.refreshDocumentRecords();
  }
  public label(fl: DocumentRecord): void {
    this.router.navigate(['/training/labeling', fl.id]);
  }
  public remove(fileItem: DocumentRecord): void {
    const index: number = this.fileList.indexOf(fileItem);
    if (index !== -1) {
      this.fileListIsLoading = true;
      this.dataService.removeDocumentRecord([{id: fileItem.id}])
        .finally(() => {
          this.fileListIsLoading = false;
        })
        .subscribe(
        data => {
          this.messagebox.success(Messages.SCS_FL_SCS_RMV.replace('{{fname}}', fileItem.name));
          this.refreshDocumentRecords();
        },
        err => {
          this.messagebox.error(Messages.ERR_NOT_REMOVED);
        }
      );
    }
  }
  public fileChange(event): void {
    const fileList: any[] = event.target.files;
    if (!fileList || !fileList.length) {
      return;
    }

    let empty = true;
    for (const k in fileList) {
      if (fileList.hasOwnProperty(k)
        && this.allowedMimeTypes.indexOf(fileList[k].type) !== -1
        && this.fileList.indexOf(fileList[k]) === -1) {
        empty = false;
        this.fileList.unshift({
          name: fileList[k].name.substring(0, fileList[k].name.lastIndexOf('.')),
          file: fileList[k],
          isSuccess: false,
          isUploading: false,
          progress: 0
        });
        this.fileList = this.fileList.slice();
      }
    }
    if (empty) {
      this.messagebox.error(Messages.ERR_ONLY_DOC);
      return;
    }

    async.map(this.fileList, (item: DocumentRecord, next: Function) => {
      if (!item.file || item.isSuccess || item.isUploading) {
        return next();
      }
      item.isUploading = true;
      this.uploadOne(item, err => {
        item.isUploading = false;
        if (!err) {
          item.isSuccess = true;
          this.messagebox.success(Messages.SCS_FL_SCS_UPL.replace('{{fname}}', item.file.name));
        } else {
          this.messagebox.error(Messages.ERR_WHILE_UPLOADING.replace('{{fname}}', item.file.name));
        }
        next();
      });
    }, () => {
      event.target.value = '';
      this.fileList = this.fileList.slice();
    });
  }

  // Private methods
  private refreshDocumentRecords(): void {
    this.fileListIsLoading = true;
    this.dataService.getDocumentRecords()
      .finally(() => {
        this.fileListIsLoading = false;
      })
      .subscribe(
      (data: any) => {
        this.fileList = data;
      },
      err => {
        this.messagebox.error(Messages.ERR_GETTING_FILELIST);
      }
    );
  }
  private uploadOne(fileItem: DocumentRecord, callback: Function): void {
    this.dataService.createDocumentRecords([{name: fileItem.name}]).subscribe(
      data => {
        fileItem.id = data[0].id;
        fileItem.progress = 0;
        this.dataService.uploadDocumentRecordContent(fileItem).subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            fileItem.progress = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            callback();
          } else if (event instanceof HttpHeaderResponse) {
            this.removeInvalidItem(fileItem, () => {
              callback(event);
            });
          }
        });
      },
      err => {
        callback(err);
      }
    );
  }
  private removeInvalidItem(fileItem: DocumentRecord, callback: Function): void {
    const index: number = this.fileList.indexOf(fileItem);
    if (index !== -1) {
      this.fileList.splice(index, 1);
      this.dataService.removeDocumentRecord([{id: fileItem.id}])
        .finally(() => {
          callback();
        })
        .subscribe(
        dt => {},
        err => {
          console.log(err);
        }
      );
    }
  }
}
