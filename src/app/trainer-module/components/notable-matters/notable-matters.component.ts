import { Component, OnInit } from '@angular/core';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import { DataService } from '../../../shared-module/services/data.service';
import { Util, SPLIT_CHARACTER } from '../../../helpers/util';
import { NotableMatter } from '../../../shared-module/models/notable-matter.model'
import { Provision, ProvisionLabel } from '../../../shared-module/models/provision.model'
import { TransactionTypeLabel, TransactionType } from '../../../shared-module/models/transaction-type.model'
@Component({
  selector: 'app-notable-matters',
  templateUrl: './notable-matters.component.html',
  styleUrls: ['./notable-matters.component.scss']
})
export class NotableMattersComponent implements OnInit {
  public transactionTypes: TransactionTypeLabel[];
  public notableMatters: NotableMatter[];
  public provisions: ProvisionLabel[];
  public selectedNotableMatter: NotableMatter = {};
  public selectedProvision: Provision = {};
  public selectedTransactionType: TransactionType = {};
  public notableMattersLoading: boolean = true;
  public showTypeEditWindow: boolean = false;
  constructor(public messagebox: MessageboxService,
              public dataService: DataService) { }

  ngOnInit() {
    this.dataService.getTransactionTypes().subscribe(
      (data: any) => {
        this.transactionTypes = [];
        data.forEach(item => {
          this.transactionTypes.push({
            label: item.name,
            value: item
          });
        });
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_TTS);
      }
    );
    this.dataService.getProvisions().subscribe(
      (data: any) => {
        this.provisions = [];
        data.forEach(item => {
          this.provisions.push({
            label: item.name,
            value: item
          });
        });
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_PVS);
      }
    );
    this.refreshNotableMatters();
  }
  public newNotableMatter(): void {
    this.clearSelectedFields();
    this.showTypeEditWindow = true;
  }
  public saveNotableMatter(notableMatter: NotableMatter): void {
    if (!notableMatter.name) {
      this.messagebox.warning(Messages.WRN_PLS_FLL_NMN);
      return;
    }
    if (!this.selectedTransactionType || !this.selectedTransactionType.id) {
      this.messagebox.warning(Messages.WRN_PLS_SLC_TT);
      return;
    }
    if (!notableMatter.definition || !notableMatter.definition.length) {
      this.messagebox.warning(Messages.WRN_PLS_FLL_DF);
      return;
    }
    if (!Util.isValidDefinition(notableMatter.definition)) {
      this.messagebox.warning(Messages.WRN_IVD_DF_SYN);
      return;
    }
    notableMatter.transaction_type = this.selectedTransactionType.id;
    this.showTypeEditWindow = false;
    this.notableMattersLoading = true;
    const newObj = Object.assign({}, notableMatter);
    delete newObj.displayDefinition;
    // Update
    if (newObj.id) {
      this.dataService.updateNotableMatter([newObj])
        .finally(() => {
          this.notableMattersLoading = false;
        })
        .subscribe(
        data => {
          this.messagebox.success(Messages.SCS_NM_SCS_UPDT);
          this.refreshNotableMatters();
        },
        err => {
          this.messagebox.error(Messages.ERR_CNT_UPDT_NM);
        }
      );
      return;
    }
    // Create new
    this.dataService.createNotableMatter([newObj])
      .finally(() => {
        this.notableMattersLoading = false;
      })
      .subscribe(
      data => {
        this.messagebox.success(Messages.SCS_NM_SCS_CRT);
        this.refreshNotableMatters();
      },
      err => {
        this.messagebox.error(Messages.ERR_CNT_CRT_NM);
      }
    );
    return;
  }
  public editNotableMatter(nm: NotableMatter): void {
    this.clearSelectedFields();
    this.selectedNotableMatter = Object.assign({}, nm);
    this.selectedNotableMatter.displayDefinition = this.convertToDisplay(this.selectedNotableMatter.definition);
    this.selectedTransactionType = (this.transactionTypes.find(item => item.value.id === nm.transaction_type) || {}).value || {};
    this.showTypeEditWindow = true;
  }
  public removeNotableMatter(nm: NotableMatter): void {
    this.notableMattersLoading = true;
    this.dataService.removeNotableMatter([nm]).subscribe(
      data => {
        const index: number = this.notableMatters.indexOf(nm);
        if (index !== -1) {
          this.notableMatters.splice(index, 1);
          this.notableMatters = this.notableMatters.slice();
        }
        this.notableMattersLoading = false;
        this.messagebox.success(Messages.SCS_NM_SCS_RMV);
      },
      err => {
        if (err.status === 409 ) {
          this.messagebox.error(Messages.ERR_NM_HAS_REL);
          return;
        }
        this.messagebox.error(Messages.ERR_CNT_RM_NM);
      }
    );
  }
  public getTransactionName(nm: NotableMatter): string {
    if (!this.transactionTypes || !this.transactionTypes.length) {
      return;
    }
    const tt = this.transactionTypes.find(item => item.value.id === nm.transaction_type);
    return tt && tt.value ? tt.value.name : '';
  }
  public getProvisionName(id: string): string {
    if (!this.provisions || !this.provisions.length) {
      return;
    }
    const dt = this.provisions.find(item => item.value.id === id);
    return dt && dt.value ? dt.value.name : '';
  }
  public insertProvision(provision: Provision): void {
    if (!this.selectedNotableMatter.definition || !this.selectedNotableMatter.definition.length) {
      this.selectedNotableMatter.definition = String(provision.id);
      this.selectedNotableMatter.displayDefinition = this.convertToDisplay(this.selectedNotableMatter.definition);
      return;
    }
    const lastElement = this.selectedNotableMatter.definition.split(SPLIT_CHARACTER).pop();
    if (lastElement && !Util.isLogicalOperator(lastElement) && lastElement !== '(') {
      this.messagebox.warning(Messages.WRN_PLS_INST_LO);
      return;
    }
    this.selectedNotableMatter.definition += SPLIT_CHARACTER + provision.id;
    this.selectedNotableMatter.displayDefinition = this.convertToDisplay(this.selectedNotableMatter.definition);
  }
  public insertLogicalOperator(operatorName: string): void {
    if (!this.selectedNotableMatter.definition || !this.selectedNotableMatter.definition.length) {
      if (operatorName === '( )') {
        this.selectedNotableMatter.definition = '(';
      } else if (operatorName === 'NOT') {
        this.selectedNotableMatter.definition = 'NOT';
      }
      this.selectedNotableMatter.displayDefinition = this.convertToDisplay(this.selectedNotableMatter.definition);
      return;
    }
    const lastElement = this.selectedNotableMatter.definition.split(SPLIT_CHARACTER).pop();
    switch (operatorName) {
      case 'Clear':
        this.selectedNotableMatter.definition = '';
        break;
      case 'NOT':
        if (lastElement && !Util.isLogicalOperator(lastElement) && lastElement !== '(') {
          this.messagebox.warning(Messages.WRN_NT_PSB_NOT);
          return;
        }
        this.selectedNotableMatter.definition += SPLIT_CHARACTER + operatorName;
        break;
      case '( )':
        if (!lastElement || Util.isLogicalOperator(lastElement)) {
          this.selectedNotableMatter.definition += SPLIT_CHARACTER + '(';
          break;
        }
        if (this.selectedNotableMatter.definition.split(SPLIT_CHARACTER).filter(item => item === '(').length >
          this.selectedNotableMatter.definition.split(SPLIT_CHARACTER).filter(item => item === ')').length) {
          this.selectedNotableMatter.definition += SPLIT_CHARACTER + ')';
        }
        break;
      case 'AND':
      case 'OR':
        if (!this.provisions.length) {
          return;
        }
        if (!lastElement || Util.isLogicalOperator(lastElement) || lastElement === '(' ) {
          this.messagebox.warning(Messages.WRN_PLS_INST_PV);
          return;
        }
        this.selectedNotableMatter.definition += SPLIT_CHARACTER + operatorName;
        break;
    }
    this.selectedNotableMatter.displayDefinition = this.convertToDisplay(this.selectedNotableMatter.definition);
  }

  // Private methods
  private clearSelectedFields(): void {
    this.selectedNotableMatter = {};
    this.selectedProvision = {};
    this.selectedTransactionType = {};
  }
  private convertToDisplay(definition: string): string {
    let result = '';
    const definitionArray = definition ? definition.split(SPLIT_CHARACTER) : [];
    definitionArray.forEach(item => {
      switch (item) {
        case ' ':
          break;
        case 'AND':
        case 'OR':
        case 'NOT':
        case '(':
        case ')':
          result += '<u>' + item + '</u>';
          break;
        default:
          result += '<b>' + this.getProvisionName(item) + '</b>';
      }
    });
    return result;
  }
  private refreshNotableMatters(): void {
    this.notableMattersLoading = true;
    this.dataService.getNotableMatters()
      .finally(() => {
        this.notableMattersLoading = false;
      })
      .subscribe(
      (data: any) => {
        this.notableMatters = data;
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_NM);
      }
    );
  }
}
