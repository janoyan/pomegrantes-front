import { Component, OnInit } from '@angular/core';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import { DataService } from '../../../shared-module/services/data.service';
import { Util, SPLIT_CHARACTER} from '../../../helpers/util';
import { DocumentTypeLabel, DocumentType } from '../../../shared-module/models/document-type.model'
import { TransactionType } from '../../../shared-module/models/transaction-type.model'

@Component({
  selector: 'app-transaction-types',
  templateUrl: './transaction-types.component.html',
  styleUrls: ['./transaction-types.component.scss']
})
export class TransactionTypesComponent implements OnInit {
  public transactionTypes: TransactionType[];
  public documentTypes: DocumentTypeLabel[];
  public availableDocumentTypes: DocumentTypeLabel[];
  public selectedType: TransactionType = {};
  public selectedDocumentType: DocumentTypeLabel = {};
  public transactionTypesLoading: boolean = true;
  public showTypeEditWindow: boolean = false;

  constructor(public messagebox: MessageboxService,
              public dataService: DataService) { }

  ngOnInit() {
    this.refreshTransactionTypes();
    this.dataService.getDocumentTypes().subscribe(
      (data: any) => {
        this.documentTypes = [];
        data.forEach(item => {
          this.documentTypes.push({
            label: item.name,
            value: item
          });
        });
        this.availableDocumentTypes = Object.assign([], this.documentTypes);
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_DTS);
      }
    );
  }
  public newTransactionType(): void {
    this.showTypeEditWindow = true;
    this.availableDocumentTypes = Object.assign([], this.documentTypes);
    this.clearSelectedFields();
  }
  public editTransactionType(tt: TransactionType): void {
    this.clearSelectedFields();
    this.selectedType = Object.assign({}, tt);
    this.selectedType.displayDefinition = this.convertToDisplay(this.selectedType.definition);
    this.showTypeEditWindow = true;
  }
  public saveTransactionType(tt: TransactionType): void {
    if (!tt.name) {
      this.messagebox.warning(Messages.WRN_PLS_FLL_TTN);
      return;
    }
    if (!tt.definition || !tt.definition.length) {
      this.messagebox.warning(Messages.WRN_PLS_FLL_DF);
      return;
    }
    if (!Util.isValidDefinition(tt.definition)) {
      this.messagebox.warning(Messages.WRN_IVD_DF_SYN);
      return;
    }
    this.showTypeEditWindow = false;
    this.transactionTypesLoading = true;
    const newObj = Object.assign({}, tt);
    delete newObj.displayDefinition;
    // Update
    if (newObj.id) {
      this.dataService.updateTransactionType([newObj])
        .finally(() => {
          this.transactionTypesLoading = false;
        })
        .subscribe(
        data => {
          this.messagebox.success(Messages.SCS_TT_SCS_UPDT);
          this.refreshTransactionTypes();
        },
        err => {
          this.messagebox.error(Messages.ERR_CNT_UPDT_TT);
        }
      );
      return;
    }
    // Create New
    this.dataService.createTransactionType([newObj])
      .finally(() => {
        this.transactionTypesLoading = false;
      })
      .subscribe(
      data => {
        this.messagebox.success(Messages.SCS_TT_SCS_CRT);
        this.refreshTransactionTypes();
      },
      err => {
        this.messagebox.error(Messages.ERR_CNT_CRT_TT);
      }
    );
    return;
  }
  public removeTransactionType(tt: TransactionType): void {
    this.transactionTypesLoading = true;
    this.dataService.removeTransactionType([tt])
      .finally(() => {
        this.transactionTypesLoading = false;
      })
      .subscribe(
      data => {
        this.showTypeEditWindow = false;
        this.messagebox.success(Messages.SCS_TT_SCS_RMV);
        this.refreshTransactionTypes();
      },
      err => {
        if (err.status === 409 ) {
          this.messagebox.error(Messages.ERR_TT_HAS_REL);
          return;
        }
        this.messagebox.error(Messages.ERR_CNT_RM_TT);
      }
    );
  }
  public insertDocumentType(selectedDocumentType: DocumentType): void {
    if (!this.selectedType.definition || !this.selectedType.definition.length) {
      this.selectedType.definition = String(selectedDocumentType.id);
      this.selectedType.displayDefinition = this.convertToDisplay(this.selectedType.definition);
      return;
    }
    const lastElement = this.selectedType.definition.split(SPLIT_CHARACTER).pop();
    if (lastElement && !Util.isLogicalOperator(lastElement) && lastElement !== '('  && lastElement !== 'NOT') {
      this.messagebox.warning(Messages.WRN_PLS_INST_LO);
      return;
    }
    this.selectedType.definition += SPLIT_CHARACTER + selectedDocumentType.id;
    this.selectedType.displayDefinition = this.convertToDisplay(this.selectedType.definition);
  }
  public insertLogicalOperator(operatorName: string): void {
    if (!this.selectedType.definition || !this.selectedType.definition.length) {
      if (operatorName === '( )') {
        this.selectedType.definition = '(';
      } else if (operatorName === 'NOT') {
        this.selectedType.definition = 'NOT';
      }
      this.selectedType.displayDefinition = this.convertToDisplay(this.selectedType.definition);
      return;
    }
    const lastElement = this.selectedType.definition.split(SPLIT_CHARACTER).pop();
    switch (operatorName) {
      case 'Clear':
        this.selectedType.definition = '';
        break;
      case 'NOT':
        if (lastElement && !Util.isLogicalOperator(lastElement) && lastElement !== '(') {
          this.messagebox.warning(Messages.WRN_NT_PSB_NOT);
          return;
        }
        this.selectedType.definition += SPLIT_CHARACTER + operatorName;
        break;
      case '( )':
        if (!lastElement || Util.isLogicalOperator(lastElement)) {
          this.selectedType.definition += SPLIT_CHARACTER + '(';
          break;
        }
        if (this.selectedType.definition.split(SPLIT_CHARACTER).filter(item => item === '(').length >
          this.selectedType.definition.split(SPLIT_CHARACTER).filter(item => item === ')').length) {
          this.selectedType.definition += SPLIT_CHARACTER + ')';
        }
        break;
      case 'AND':
      case 'OR':
        if (!this.availableDocumentTypes.length) {
          return;
        }
        if (!lastElement || Util.isLogicalOperator(lastElement) || lastElement === '(' ) {
          this.messagebox.warning(Messages.WRN_PLS_INST_DT);
          return;
        }
        this.selectedType.definition += SPLIT_CHARACTER + operatorName;
        break;
    }
    this.selectedType.displayDefinition = this.convertToDisplay(this.selectedType.definition);
  }
  public onDropdownFocus(): void {
    if (!this.availableDocumentTypes.length) {
      this.messagebox.warning(Messages.WRN_NO_AVLB_DT);
    }
  }

  // Private Methods
  private clearSelectedFields(): void {
    this.selectedType = {};
    this.selectedDocumentType = {};
  }
  private convertToDisplay(definition): string {
    let result: string = '';
    this.availableDocumentTypes = Object.assign([], this.documentTypes);
    definition = definition ? definition.split(' ') : [];
    definition.forEach(item => {
      switch (item) {
        case ' ':
          break;
        case 'AND':
        case 'OR':
        case 'NOT':
        case '(':
        case ')':
          result += '<u>' + item + '</u>';
          break;
        default:
          result += '<b>' + this.getDocumentTypeName(item) + '</b>';
      }
    });
    return result;
  }
  private getDocumentTypeName(id: string): string  {
    const dt = this.documentTypes.find(item => item.value.id === id);
      return dt && dt.value ? dt.value.name : '';
  }
  private refreshTransactionTypes(): void {
    this.transactionTypesLoading = true;
    this.dataService.getTransactionTypes()
      .finally(() => {
        this.transactionTypesLoading = false;
      })
      .subscribe(
      (data: any) => {
        this.transactionTypes = data;
      },
      err => {
        this.messagebox.error(Messages.ERR_CNT_LOAD_TTS);
      }
    );
  }
}
