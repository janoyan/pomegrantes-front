import { Component, OnInit } from '@angular/core';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import { DataService } from '../../../shared-module/services/data.service';
import { DocumentType } from '../../../shared-module/models/document-type.model'

@Component({
  selector: 'app-document-types',
  templateUrl: './document-types.component.html',
  styleUrls: ['./document-types.component.scss']
})
export class DocumentTypesComponent implements OnInit {
  public documentTypes: DocumentType[];
  public selectedType: DocumentType = {};
  public documentTypesLoading: boolean = false;
  public showTypeEditWindow: boolean = false;

  constructor(public messagebox: MessageboxService,
              public dataService: DataService) {}
  ngOnInit() {
    this.refreshDocumentTypes();
  }
  public newDocumentType(): void {
    this.showTypeEditWindow = true;
    this.selectedType = {};
  }
  public editDocumentType(dt: DocumentType): void {
    this.selectedType = Object.assign({}, dt);
    this.showTypeEditWindow = true;
  }
  public saveDocumentType(dt: DocumentType): void {
    if (!dt.name) {
      this.messagebox.warning(Messages.WRN_PLS_FLL_DTN);
      return;
    }
    // Update
    this.documentTypesLoading = true;
    this.showTypeEditWindow = false;
    if (dt.id) {
      this.dataService.updateDocumentType([dt])
        .finally(() => {
          this.documentTypesLoading = false;
        })
        .subscribe(
        data => {
          this.messagebox.success(Messages.SCS_DT_SCS_UPDT);
          this.refreshDocumentTypes();
        },
        err => {
          this.messagebox.error(Messages.ERR_CNT_UPDT_DT);
        }
      );
      return;
    }
    // Create New
    this.dataService.createDocumentTypes([dt])
      .finally(() => {
        this.documentTypesLoading = false;
      })
      .subscribe(
      data => {
        this.messagebox.success(Messages.SCS_DT_SCS_CRT);
        this.refreshDocumentTypes();
      },
      err => {
        this.messagebox.error(Messages.ERR_CNT_CRT_DT);
      }
    );
    return;
  }
  public removeDocumentType(dt: DocumentType): void {
    this.documentTypesLoading = true;
    this.dataService.removeDocumentType([{id: dt.id}]).subscribe(
      data => {
        this.messagebox.success(Messages.SCS_DT_SCS_RMV);
        this.refreshDocumentTypes();
      },
      err => {
        if (err.status === 409 ) {
          this.messagebox.error(Messages.ERR_DT_HAS_REL);
          return;
        }
        this.messagebox.error(Messages.ERR_CNT_RM_DT);
      }
    );
  }
  public retrainDocumentType(documentType: DocumentType): void {
    this.messagebox.showFullPreloader(Messages.INF_RTR_DT);
    this.dataService.retrainDocumentType({documentTypeId: documentType.id})
      .finally(() => {
        this.messagebox.hideFullPreloader();
      })
      .subscribe(
        () => {
          documentType.has_latest_model = true;
        },
        err => {
          this.messagebox.error(Messages.ERR_CNT_RTR_DT)
        }
      );
  }

  // Private methods
  private refreshDocumentTypes(): void {
    this.documentTypesLoading = true;
    this.dataService.getDocumentTypes()
      .finally(() => {
        this.documentTypesLoading = false;
      })
      .subscribe(
      (data: any) => {
        this.documentTypes = data;
        },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_DTS);
      }
    );
  }
}
