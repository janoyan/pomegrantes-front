import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../../../shared-module/services/data.service';
import {MessageboxService} from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import * as _ from 'lodash';
import * as async from 'async';
import {DocumentType} from '../../../shared-module/models/document-type.model';
import {Provision} from '../../../shared-module/models/provision.model';
import {ProvisionRecord} from '../../../shared-module/models/provision-record.model';
import {DocumentRecord} from '../../../shared-module/models/document-record.model';
import {DocumentTypeTrainingEntrie} from '../../../shared-module/models/document-type-training-entrie.model';
import {EntrieContainer} from '../../../shared-module/types/entrie-container.class';
import {PdfViewComponent} from '../../../shared-module/components/pdf-view.component';

@Component({
  selector: 'app-labeling',
  templateUrl: './labeling.component.html',
  styleUrls: ['./labeling.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})
export class LabelingComponent implements OnInit, OnDestroy {
  public file: DocumentRecord;
  public DTTEContainer: EntrieContainer<DocumentTypeTrainingEntrie>;
  public availableDocumentTypes: DocumentType[] = [];
  public documentTypes: DocumentType[] = [];
  public provisions: Provision[] = [];
  public availableProvisions: Provision[] = [];
  public provisionRecords: ProvisionRecord[] = [];
  public popupLef: number;
  public popupTop: number;
  public dPopupLef: number;
  public dPopupTop: number;
  public showProvisionPopup: boolean = false;
  public showProvisionListPopup: boolean = false;
  public showDocumentTypePopup: boolean = false;
  public textLevelHeight: number = 600;
  public selectedDocumentType: DocumentType;
  public newProvisionRecord: ProvisionRecord;
  public pdfView: PdfViewComponent;
  private routerSub: any;
  private _shouldLoadModules: number = 5;
  private selectedText: string;
  private needToHighlight: number[][];
  private get shouldLoadModules(): number {
    return this._shouldLoadModules;
  }
  private set shouldLoadModules(val: number) {
    if (val === 0) {
      this.messagebox.hideFullPreloader();
    }
    this._shouldLoadModules = val;
  }

  constructor(public route: ActivatedRoute,
              public messagebox: MessageboxService,
              public dataService: DataService) {
    this.messagebox.showFullPreloader();
  }

  ngOnInit() {
    this.routerSub = this.route.params.subscribe(params => {
      const documentId: string = params['id'];
      const provisionRecordId: string = params['prId'];
      if (provisionRecordId) {
        this.dataService.getProvisionRecordById({provisionRecordId: provisionRecordId}).subscribe(
          (data: any) => {
            this.needToHighlight = data.coords;
          },
          err => {
            this.messagebox.error(Messages.ERR_CNT_GET_PR)
          });
      }
      this.initializeDocumentData(documentId);
    });
    this.onResize();
  }

  ngOnDestroy() {
    this.routerSub.unsubscribe();
    this.messagebox.hideFullPreloader();
  }

  public onPdfInit(pdfView: PdfViewComponent) {
    this.pdfView = pdfView;
    pdfView.setToken(this.dataService.getToken());
  }

  public textSelected(e): void {
    this.pdfView.hideHighlight();
    const selectedText = window.getSelection().toString();
    const SIZE = 150;
    if (selectedText && selectedText.length) {
      this.newProvisionRecord = {
        selected_text: selectedText,
        in_document_record: this.file.id,
        for_provisions: [],
        coords: this.pdfView.getHightlightCoords()
      };
      this.popupLef = e.clientX - SIZE;
      this.popupTop = window.outerHeight && ((window.outerHeight - e.clientY) > (SIZE * 2)) ? e.clientY : e.clientY - SIZE;
      this.selectedText = selectedText;
      this.showProvisionPopup = true;
    }
  }

  public showAvailableDocumentTypes(e: any): void {
    const SIZE = 150;
    this.dPopupLef = e.clientX;
    this.dPopupTop = window.outerHeight && ((window.outerHeight - e.clientY) > (SIZE * 2)) ? e.clientY : e.clientY - SIZE;
    this.availableDocumentTypes = _.filter(this.documentTypes, (item: DocumentType) =>
      !this.DTTEContainer.list.find((fitem: DocumentTypeTrainingEntrie) => fitem.document_type_id === item.id));
    if (this.availableDocumentTypes.length) {
      this.showDocumentTypePopup = true;
      return;
    }
    this.messagebox.warning(Messages.WRN_NO_AVLB_DT);
  }

  public showAvailableProvisions(e: any): void {
    this.availableProvisions = _.filter(this.provisions, (item: Provision) =>
      !this.newProvisionRecord.for_provisions.find((fitem: string) => fitem === item.id));
    if (this.availableProvisions.length) {
      this.showProvisionListPopup = true;
      return;
    }
    this.messagebox.warning(Messages.WRN_NO_AVLB_PVS);
  }

  public addDocumentType(e: any): void {
    if (e.data) {
      this.DTTEContainer.insert({
        document_id: this.file.id,
        document_type_id: e.data.id
      });
      this.selectedDocumentType = null;
      this.showDocumentTypePopup = false;
    }
  }

  public addProvision(e: any): void {
    this.showProvisionListPopup = false;
    this.newProvisionRecord.for_provisions.push(e.data.id);
  }

  public applyProvisionType(): void {
    this.showProvisionListPopup = false;
    this.showProvisionPopup = false;
    if (!this.newProvisionRecord || !this.newProvisionRecord.for_provisions.length) {
      return;
    }
    this.messagebox.showFullPreloader(Messages.INF_UPD_PVTP);
    this.dataService.createProvisionRecords([this.newProvisionRecord])
      .subscribe(
        () => {
          this.refreshLabelingData(this.file.id, true);
        },
        err => {
          this.messagebox.hideFullPreloader();
          this.messagebox.error(Messages.ERR_CNT_APPLY_PVTP);
        });
  }

  public applyDocumentTypes(): void {
    const tasks = [];
    if (this.DTTEContainer.removeList.length) {
      tasks.push(this.dataService.removeDTTEntries.bind(this.dataService, this.DTTEContainer.removeList));
    }
    if (this.DTTEContainer.insertList.length) {
      tasks.push(this.dataService.createDTTEntries.bind(this.dataService, this.DTTEContainer.insertList));
    }
    this.messagebox.showFullPreloader(Messages.INF_UPD_DTTE);
    async.mapSeries(tasks, (task: Function, next: Function) => {
      task().subscribe(
        data => next(),
        err => next(err)
      );
    }, err => {
      if (err) {
        this.messagebox.hideFullPreloader();
        return this.messagebox.error(Messages.ERR_CNT_UPD_DTTE);
      }
      this.dataService.getDTTEntriesByDocumentId({documentId: this.file.id})
        .finally(() => this.messagebox.hideFullPreloader())
        .subscribe(
          (data: any) => {
            this.DTTEContainer.reset(data);
            this.messagebox.hideFullPreloader();
            this.messagebox.success(Messages.SCS_DTTE_SCS_UPDT);
          },
          error => this.messagebox.error(Messages.ERR_CNT_LOAD_DTTE)
        );
    });

  }

  public getDocumentTypeName(documentTypeId: string): string {
    if (!this.documentTypes || !this.documentTypes.length) {
      return '';
    }
    const tt = this.documentTypes.find(item => item.id === documentTypeId);
    return tt ? tt.name : '';
  }

  public getProvisionName(provisionId: string): string {
    if (!this.provisions || !this.provisions.length) {
      return '';
    }
    const pv = this.provisions.find(item => item.id === provisionId);
    return pv ? pv.name : '';
  }

  public navigateToSee(coords: number[][]): void {
    this.pdfView.hideHighlight();
    this.pdfView.showHighlight(coords);
  }

  public removeProvisionRecord(id: string): void {
    this.pdfView.hideHighlight();
    this.messagebox.showFullPreloader(Messages.INF_RMV_PVR);
    this.dataService.removeProvisionRecord([{id: id}]).finally(() => {
      this.refreshLabelingData(this.file.id, true);
    }).subscribe();
  }

  // Private Methods
  private initializeDocumentData(documentRecordId: string): void {
    this.dataService.getDocumentRecordById({id: documentRecordId})
      .finally(() => this.shouldLoadModules--)
      .subscribe(
        data => {
          this.file = data;
          this.pdfView.renderPdf(this.dataService.getPdfUrl(this.file.id), err => {
            if (err) {
              this.messagebox.hideFullPreloader();
              this.messagebox.error(err.message);
              return;
            }
            this.shouldLoadModules--;
            if (this.needToHighlight) {
              this.pdfView.showHighlight(this.needToHighlight);
            }
          });
        },
        err => {
          this.messagebox.error(Messages.ERR_CNT_GET_DOC);
        }
      );
    this.dataService.getDocumentTypes()
      .finally(() => this.shouldLoadModules--)
      .subscribe(
        (data: any) => {
          this.documentTypes = data || [];
        },
        err => {
          this.messagebox.hideFullPreloader();
          this.messagebox.error(Messages.ERR_CNT_LOAD_DTS);
        }
      );
    this.dataService.getProvisions()
      .finally(() => this.shouldLoadModules--)
      .subscribe(
        (data: any) => {
          this.provisions = data || [];
        },
        err => {
          this.messagebox.hideFullPreloader();
          this.messagebox.error(Messages.ERR_CNT_LOAD_PVS);
        }
      );
    this.refreshLabelingData(documentRecordId);
  }

  private refreshLabelingData(documentRecordId: string, hidePreloader?: boolean): void {
    this.dataService.getDocumentRecordLabelingInfo({documentId: documentRecordId})
      .finally(() => {
        if (!hidePreloader) {
          this.shouldLoadModules--;
        } else {
          this.messagebox.hideFullPreloader();
        }
      })
      .subscribe(
        (data: any) => {
          this.DTTEContainer = new EntrieContainer<DocumentTypeTrainingEntrie>(data.training_entries);
          this.provisionRecords = data.provision_records;
        },
        err => {
          this.messagebox.hideFullPreloader();
          this.messagebox.error(Messages.ERR_CNT_LOAD_LBL);
        }
      );
  }

  private onResize(): void {
    this.textLevelHeight = window.innerHeight - 60;
  }
}
