import { Component, OnInit } from '@angular/core';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import { DataService } from '../../../shared-module/services/data.service';
import { Topic, TopicLabel } from '../../../shared-module/models/topic.model';
import { TransactionTypeLabel, TransactionType } from '../../../shared-module/models/transaction-type.model';
import { Provision, ProvisionLabel } from '../../../shared-module/models/provision.model';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.scss']
})
export class TopicsComponent implements OnInit {
  public transactionTypes: TransactionTypeLabel[];
  public topics: Topic[];
  public availableTopics: TopicLabel[];
  public provisions: ProvisionLabel[];
  public availableProvisions: ProvisionLabel[];
  public selectedTopic: Topic = {};
  public selectedProvision: Provision = {};
  public selectedParentTopic: Topic = {};
  public selectedTransactionType: TransactionType = {};
  public topicsLoading: boolean = true;
  public showTypeEditWindow: boolean = false;
  constructor(public messagebox: MessageboxService,
              public dataService: DataService) { }

  ngOnInit() {
    this.dataService.getTransactionTypes().subscribe(
      (data: any) => {
        this.transactionTypes = [];
        data.forEach(item => {
          this.transactionTypes.push({
            label: item.name,
            value: item
          });
        });
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_TTS);
      }
    );
    this.dataService.getProvisions().subscribe(
      (data: any) => {
        this.provisions = [];
        data.forEach(item => {
          this.provisions.push({
            label: item.name,
            value: item
          });
        });
        this.availableProvisions = Object.assign([], this.provisions);
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_PVS);
      }
    );
    this.refreshTopics();
  }
  public newTopic(): void {
    this.clearSelectedFields();
    this.availableProvisions = Object.assign([], this.provisions);
    this.availableTopics = [];
    this.topics.forEach(item => {
      this.availableTopics.push({
        label: item.name,
        value: item
      });
    });
    this.showTypeEditWindow = true;
  }
  public editTopic(tp: Topic): void {
    this.clearSelectedFields();
    this.selectedTopic = Object.assign({}, tp);
    this.selectedTopic.displayDefinition = this.convertToDisplay(this.selectedTopic.definition);
    this.selectedTransactionType = (this.transactionTypes.find(item => item.value.id === tp.transaction_type) || {}).value || {};
    this.selectedParentTopic = (this.availableTopics.find(item => item.value.id === tp.child_of) || {}).value || {};
    this.availableTopics = [];
    this.topics.forEach(item => {
      if (item.id === tp.id) {
        return;
      }
      this.availableTopics.push({
        label: item.name,
        value: item
      });
    });
    this.showTypeEditWindow = true;
  }
  public getTransactionName(tp: Topic): string {
    if (this.transactionTypes && this.transactionTypes.length) {
      const tt = this.transactionTypes.find(item => item.value.id === tp.transaction_type);
      return tt && tt.value ? tt.value.name : '';
    }
    return '';
  }
  public saveTopic(topic: Topic): void {
    if (!topic.name) {
      this.messagebox.warning(Messages.WRN_PLS_FLL_TPN);
      return;
    }
    if (!this.selectedTransactionType || !this.selectedTransactionType.id) {
      this.messagebox.warning(Messages.WRN_PLS_SLC_TT);
      return;
    }
    if (!topic.definition || !topic.definition.length) {
      this.messagebox.warning(Messages.WRN_PLS_FLL_DF);
      return;
    }
    const newObj = Object.assign({}, topic);
    delete newObj.displayDefinition;
    newObj.transaction_type = this.selectedTransactionType.id;
    newObj.child_of = this.selectedParentTopic.id;
    this.topicsLoading = true;
    this.showTypeEditWindow = false;
    // Update
    if (newObj.id) {
      this.dataService.updateTopic([newObj])
        .finally(() => {
          this.topicsLoading = false;
        })
        .subscribe(
        data => {
          this.messagebox.success(Messages.SCS_TP_SCS_UPDT);
          this.refreshTopics();
        },
        err => {
          this.messagebox.error(Messages.ERR_CNT_UPDT_TP);
        }
      );
      return;
    }
    // Create new
    this.dataService.createTopic([newObj])
      .finally(() => {
        this.topicsLoading = false;
      })
      .subscribe(
      data => {
        this.messagebox.success(Messages.SCS_TP_SCS_CRT);
        this.refreshTopics();
      },
      err => {
        this.messagebox.error(Messages.ERR_CNT_CRT_TP);
      }
    );
    return;
  }
  public removeTopic(tp: Topic): void {
    this.topicsLoading = true;
    this.dataService.removeTopic([{id: tp.id}])
      .finally(() => {
        this.topicsLoading = false;
      })
      .subscribe(
      data => {
        this.messagebox.success(Messages.SCS_TP_SCS_RMV);
        this.refreshTopics();
      },
      err => {
        if (err.status === 409 ) {
          this.messagebox.error(Messages.ERR_PV_HAS_REL);
          return;
        }
        this.messagebox.error(Messages.ERR_CNT_RM_TP);
      }
    );
  }
  public insertProvision(provision: Provision): void {
    this.selectedTopic.definition = this.selectedTopic.definition || [];
    this.selectedTopic.definition.push(provision.id);
    this.removeElementFromProvisions(provision.id);
    this.selectedProvision = {};
    this.selectedTopic.displayDefinition = this.convertToDisplay(this.selectedTopic.definition);
  }
  public clearDefinition(): void {
    this.selectedTopic.definition = [];
    this.selectedTopic.displayDefinition = this.convertToDisplay(this.selectedTopic.definition);
  }
  public onProvisionDropdownFocus(): void {
    if (!this.availableProvisions.length) {
      this.messagebox.warning(Messages.WRN_NO_AVLB_PVS);
    }
  }
  public onParentDropdownFocus(): void {
    if (!this.topics || !this.topics.length) {
      this.messagebox.warning(Messages.WRN_NO_AVLB_TPS);
    }
  }
  public onTransactionDropdownFocus(): void {
    if (!this.transactionTypes || !this.transactionTypes.length) {
      this.messagebox.warning(Messages.WRN_NO_AVLB_TT);
    }
  }

  // Private methods
  private clearSelectedFields(): void {
    this.selectedTopic = {};
    this.selectedParentTopic = {};
    this.selectedTransactionType = {};
  }
  private convertToDisplay(definition: string[]): string {
    let result = '';
    definition = definition || [];
    this.availableProvisions = Object.assign([], this.provisions);
    definition.forEach(item => {
      const provision = this.provisions.find(pItem => pItem.value.id === item);
      result += (provision && provision.value ? provision.value.name || '' : '') + ', ';
      this.removeElementFromProvisions(item);
    });
    return result.slice(0, result.length - 2);
  }
  private refreshTopics(): void {
    this.topicsLoading = true;
    this.dataService.getTopics()
      .finally(() => {
        this.topicsLoading = false;
      })
      .subscribe(
      (data: any) => {
        this.topics = data;
        this.availableTopics = [];
        data.forEach(item => {
          this.availableTopics.push({
            label: item.name,
            value: item
          });
        });
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_TP);
      }
    );
  }
  private removeElementFromProvisions(provisionId: string): void {
    const element = this.availableProvisions.find(item => item && item.value ? item.value.id === provisionId : false);
    if (element) {
      const index = this.availableProvisions.indexOf(element);
      if (index !== -1) {
        this.availableProvisions.splice(index, 1);
      }
    }
  }
}
