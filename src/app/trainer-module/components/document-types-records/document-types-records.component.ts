import { Component, OnInit, OnDestroy  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageboxService } from '../../../shared-module/services/messagebox.service';
import * as Messages from '../../../constants/messages';
import { DataService } from '../../../shared-module/services/data.service';
import { DocumentRecord } from '../../../shared-module/models/document-record.model';

@Component({
  selector: 'app-document-types-records',
  templateUrl: './document-types-records.component.html',
  styleUrls: ['./document-types-records.component.scss']
})
export class DocumentTypesRecordsComponent implements OnInit, OnDestroy {
  private routerSub: any;
  private documentTypeId: string;
  public documentRecords: DocumentRecord[] = [];
  public records: any[];
  public recordsLoading = true;
  constructor(public messagebox: MessageboxService,
              public dataService: DataService,
              public route: ActivatedRoute) {}
  ngOnInit() {
    this.routerSub = this.route.params.subscribe(params => {
      this.documentTypeId = params['id'];
      this.refreshRecord();
    });
    this.dataService.getDocumentRecords().subscribe(
      (data: any) => {
        this.documentRecords = data;
      },
      err => {
        this.messagebox.error(Messages.ERR_CNT_LOAD_DTD);
      }
    );
  }
  ngOnDestroy() {
    this.routerSub.unsubscribe();
  }
  public removeDTEntrie(dt) {
    this.recordsLoading = true;
    this.dataService.removeDTTEntries([dt])
      .finally(() => {
        this.recordsLoading = false;
      })
      .subscribe(
      data => {
        this.messagebox.success(Messages.SCS_DTD_SCS_RMV);
        this.refreshRecord();
      },
      err => {
        if (err.status === 409 ) {
          this.messagebox.error(Messages.ERR_DTR_HAS_REL);
          return;
        }
        this.messagebox.error(Messages.ERR_CNT_RM_DTD);
      }
    );
  }
  public refreshRecord() {
    this.recordsLoading = true;
    this.dataService.getDTTEntriesByDocumentTypeId({documentTypeId: this.documentTypeId})
      .finally(() => {
        this.recordsLoading = false;
      })
      .subscribe(
      (data: any) => {
        this.records = data;
      },
      err => {
        return this.messagebox.error(Messages.ERR_CNT_LOAD_DTD);
      }
    );
  }
  public getDocumentRecordName(id: string): string  {
    const dt = this.documentRecords.find(item => item.id === id);
    return dt ? dt.name : '';
  }
}
