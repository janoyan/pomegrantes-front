import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentTypesRecordsComponent } from './document-types-records.component';

describe('DocumentTypesRecordsComponent', () => {
  let component: DocumentTypesRecordsComponent;
  let fixture: ComponentFixture<DocumentTypesRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentTypesRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentTypesRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
