import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule, MatButtonModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

// PrimeNG
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { SelectButtonModule } from 'primeng/components/selectbutton/selectbutton';
import { FileUploadModule } from 'ng2-file-upload';

// COMPONENTS
import { ListComponent } from './components/list/list.component';
import { DocumentTypesComponent } from './components/document-types/document-types.component';
import { TransactionTypesComponent } from './components/transaction-types/transaction-types.component';
import { TopicsComponent } from './components/topics/topics.component';
import { ProvisionsComponent } from './components/provisions/provisions.component';
import { NotableMattersComponent } from './components/notable-matters/notable-matters.component';
import { DocumentTypesRecordsComponent } from './components/document-types-records/document-types-records.component';
import { ProvisionsRecordsComponent } from './components/provisions-records/provisions-records.component';
import { LabelingComponent } from './components/labeling/labeling.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { SharedModule } from '../shared-module/shared.module';

@NgModule({
  declarations: [
    ListComponent,
    DocumentTypesComponent,
    TransactionTypesComponent,
    TopicsComponent,
    ProvisionsComponent,
    NotableMattersComponent,
    DocumentTypesRecordsComponent,
    ProvisionsRecordsComponent,
    LabelingComponent,
    NotFoundComponent,
    TrainerComponent
  ],
  exports: [
    ListComponent,
    DocumentTypesComponent,
    TransactionTypesComponent,
    TopicsComponent,
    ProvisionsComponent,
    NotableMattersComponent,
    DocumentTypesRecordsComponent,
    ProvisionsRecordsComponent,
    LabelingComponent,
    NotFoundComponent,
    TrainerComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatButtonModule,
    RouterModule,
    FileUploadModule,
    DialogModule,
    DropdownModule,
    SelectButtonModule,
    DataTableModule
  ],
  providers: [],
  bootstrap: []
})

export class TrainerModule {}
