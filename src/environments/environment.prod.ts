export const environment = {
  production: true,
  api: {
    base: 'http://api.almond.technology/rest/',
    analyse: 'analyse/',
    documentTypes: 'document_types',
    documentType: 'document_type/',
    transactionTypes: 'transaction_types',
    notableMatters: 'notable_matters',
    withTransactionId: '/with_transaction_id/:id',
    documentRecords: 'document_records',
    provisionRecords: 'provision_records/',
    provisionRecord: 'provision_record/',
    inDocumentRecord: 'in_document_record/',
    documentContent: 'document_content/',
    provisions: 'provisions',
    provision: 'provision/',
    documentRecord: 'document_record/',
    documentRecordContent: ':id/content',
    notableMatter: 'notable_matter/',
    getByEvalution: '/get_by_evaluation/:es',
    byProvision: '/by_provision/',
    childrenOf: ':id/children',
    transactionType: 'transaction_type/',
    topics: 'topics',
    topic: 'topic/',
    documentRecordsEntrie: 'document_type_training_entries/'
  }
};
